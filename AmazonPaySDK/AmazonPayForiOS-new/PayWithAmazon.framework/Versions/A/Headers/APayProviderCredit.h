#import <Foundation/Foundation.h>
#import "APayPrice.h"

@interface APayProviderCredit : NSObject

@property (copy, nonatomic, readonly) NSString *providerID; // TODO: determine the business use caes and the proper name
@property (strong, nonatomic, readonly) APayPrice *creditAmount;

- (instancetype)init __attribute__((unavailable("Must use initWithProviderID:creditAmount: instead")));

- (instancetype)initWithProviderID:(NSString *)providerID creditAmount:(APayPrice *)creditAmount;

@end
