#import <Foundation/Foundation.h>
#import "PWAPrice.h"

__attribute__((deprecated("Use APayProviderCredit instead.")))
@interface PWAProviderCredit : NSObject

@property (copy, nonatomic, readonly) NSString *providerID; // TODO: determine the business use caes and the proper name
@property (strong, nonatomic, readonly) PWAPrice *creditAmount;

- (instancetype)init __attribute__((unavailable("Must use initWithProviderID:creditAmount: instead")));

- (instancetype)initWithProviderID:(NSString *)providerID creditAmount:(PWAPrice *)creditAmount;

@end
