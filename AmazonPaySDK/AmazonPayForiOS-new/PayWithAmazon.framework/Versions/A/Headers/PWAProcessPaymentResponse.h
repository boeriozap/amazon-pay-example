#import <Foundation/Foundation.h>
#import <PayWithAmazon/PWAWorkflowResponse.h>

__attribute__((deprecated("Use APayProcessPaymentResponse instead.")))
@interface PWAProcessPaymentResponse : NSObject <PWAWorkflowResponse>

@property (copy, nonatomic) NSString *amazonOrderReferenceId;

@end
