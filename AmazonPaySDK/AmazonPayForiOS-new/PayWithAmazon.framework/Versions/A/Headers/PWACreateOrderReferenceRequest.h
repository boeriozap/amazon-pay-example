#import <Foundation/Foundation.h>
#import "PWARegion.h"

/**
 * Request object to create a new Order Reference Object (ORO).
 */
__attribute__((deprecated("Use APayCreateOrderReferenceRequest instead.")))
@interface PWACreateOrderReferenceRequest : NSObject

/**
 * Indicates if the seller needs buyer’s shipping address to process payments.
 */
@property (assign, nonatomic) BOOL useAmazonAddressBook;

/**
 * Region override for the transaction. Default value is used if not set.
 */
@property (assign, nonatomic) PWARegion region;

/**
 * Ledger currency override for the transaction. Default value is used if not set.
 */
@property (copy, nonatomic) NSString *ledgerCurrencyCode;

/**
 * Locale override for the transaction. Default value is used if not set.
 */
@property (strong, nonatomic) NSLocale *locale;

- (instancetype)init __attribute__((unavailable("Must use initWithUseAmazonAddressBook: instead")));

/**
 * Initialize a create ORO request object with required parameters.
 *
 * @param useAmazonAddressBook      Indicates if the seller needs buyer’s shipping address to process payments.
 */
- (instancetype)initWithUseAmazonAddressBook:(BOOL)useAmazonAddressBook;

@end
