#import <Foundation/Foundation.h>
#import <PayWithAmazon/PWAWorkflowResponse.h>

@interface APayProcessPaymentResponse : NSObject <PWAWorkflowResponse>

@property (copy, nonatomic, readonly) NSString *amazonOrderReferenceId;

@end
