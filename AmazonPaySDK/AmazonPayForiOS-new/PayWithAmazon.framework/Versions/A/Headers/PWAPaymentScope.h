#import <Foundation/Foundation.h>
#import <LoginWithAmazon/LoginWithAmazon.h>

__attribute__((deprecated("Use APayPaymentScope instead.")))
@interface PWAPaymentScope : NSObject

+ (id<AMZNScope>)initiate;
+ (id<AMZNScope>)instrument;
+ (id<AMZNScope>)shippingAddress;
+ (id<AMZNScope>)billingAddress;

@end
