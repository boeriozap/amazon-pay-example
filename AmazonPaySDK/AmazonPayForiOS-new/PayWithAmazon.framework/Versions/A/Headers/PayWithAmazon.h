#import <Foundation/Foundation.h>
#import <PayWithAmazon/PWAAuthorizeAttributes.h>
#import <PayWithAmazon/PWAChangeOrderDetailsRequest.h>
#import <PayWithAmazon/PWAChangeOrderDetailsResponse.h>
#import <PayWithAmazon/PWACreateOrderReferenceRequest.h>
#import <PayWithAmazon/PWAConstants.h>
#import <PayWithAmazon/PWAError.h>
#import <PayWithAmazon/PWAPaymentScope.h>
#import <PayWithAmazon/PWAPrice.h>
#import <PayWithAmazon/PWAProcessPaymentRequest.h>
#import <PayWithAmazon/PWAProcessPaymentResponse.h>
#import <PayWithAmazon/PWAProviderAttributes.h>
#import <PayWithAmazon/PWAProviderCredit.h>
#import <PayWithAmazon/PWARegion.h>
#import <PayWithAmazon/PWASellerOrderAttributes.h>
#import <PayWithAmazon/PWAWorkflowResponse.h>
#import <PayWithAmazon/PayWithAmazonUtil.h>
#import <PayWithAmazon/AmazonPay.h>

__attribute__((deprecated("Use AmazonPay instead. PayWithAmazon is deprecated from version 2.0.3 and will be unavailable in the public release")))
@interface PayWithAmazon : NSObject

/**
 *  @name Configuration
 */

/**
 *  Sets default region to be used by Pay With Amazon services
 *
 *  @param region PWARegion indicating your region of operation
 */
+ (void)setDefaultRegion:(PWARegion)region;

/**
 *  Gets default region to be used by Pay With Amazon services
 *
 *  @return PWARegion indicating your region of operation
 */
+ (PWARegion)getDefaultRegion;

/**
 *  Sets default ledger currency code to be used by Pay With Amazon services
 *
 *  @param currencyCode NSString indicating your ledger currency code
 */
+ (void)setDefaultLedgerCurrency:(NSString *)currencyCode;

/**
 * Sets default locale to be used by payments pages to display content. If a default locale is not set or if nil locale is passed in,
 * the payment pages will use the preferred language set in the device settings
 *
 * @param locale Locale you want to use as default
 */
+ (void)setDefaultLocale:(NSLocale *)locale;

/**
 * @name Initialize payment for buyer's order
 */

/**
 * Initializes payment by creating an order reference object - a record of key attributes necessary to process the buyer's payment. Some of these
 * attributes will be determined by the buyer as they go through the checkout process (for example, shipping address and payment method) and you will
 * set some of these attributes with information about the order (for example, the total amount).  This API will return the
 * order reference identifier which you have to use for calling Pay With Amazon backend APIs. Refer Pay With Amazon Integration Guide to get more
 * details on these APIs
 *
 * @param createOrderReferenceRequest    Request object to create order reference.
 * address for processing your payment
 * @param completionHandler A Completion block to be called once order reference object is created. The block receives the following parameters:
 *              amazonOrderReferenceID     The Identifier for the order reference object created. If an error occured, this is nil.
 *              error    If an error occurred, this error object describes the error. If the operation completed successfully, this is nil.
 */
+ (void)createOrderReference:(PWACreateOrderReferenceRequest *)createOrderReferenceRequest
                        completionHandler:(void (^)(NSString *amazonOrderReferenceID, NSError *error))completionHandler;

/**
 * Allows the buyer to change their shipping address for the current transaction by redirecting the buyer to
 * amazon hosted pages where they can select a new address from their address book. Payment methods are linked to addresses, so when the shipping
 * address is changed, the buyer will also be redirected to the change payment page to change their payment method as well.
 *
 * @param changeOrderDetailsRequest  Request object to change order details.
 * @param completionHandler   A Completion block to be called once the change is made. The block receives the following parameters:
 *                              response             The PWAChangeOrderDetailsResponse object returned on request processed.
 *                                                    Response contains the amazonOrderReferenceId for which the request was processed.
 *                              error                If an error occurred, this error object describes the error. If the
 *                                                    operation completed successfully, this is nil. Incase of error, the response will contain the
 *                                                    amazonOrderReferenceId for which the request was processed.
 */
+ (void)changeShippingAddress:(PWAChangeOrderDetailsRequest *)changeOrderDetailsRequest
            completionHandler:(void (^)(PWAChangeOrderDetailsResponse *response, NSError *error))completionHandler;

/**
 * Allows the buyer to change their payment method for the current transaction by redirecting the buyer to
 * amazon hosted pages where they can select a new payment method from their wallet.
 *
 * @param changeOrderDetailsRequest  Request object to change order details.
 * @param completionHandler   A Completion block to be called once the change is made. The block receives the following parameters:
 *                              response             The PWAChangeOrderDetailsResponse object returned on request processed. 
 *                                                    Response contains the amazonOrderReferenceId for which the request was processed.
 *                              error                If an error occurred, this error object describes the error. If the
 *                                                    operation completed successfully, this is nil. Incase of error, the response will contain the
 *                                                    amazonOrderReferenceId for which the request was processed.
 */
+ (void)changePaymentMethod:(PWAChangeOrderDetailsRequest *)changeOrderDetailsRequest
              completionHandler:(void (^)(PWAChangeOrderDetailsResponse *response, NSError *error))completionHandler;

/**
 * Allows the seller's 3rd party app to initiate a request to process the buyer's payment.
 *
 * @param processPaymentRequest Request object to process payment.
 * @param completionHandler     A Completion block to be called once processing is complete. The block receives the following parameters:
 *                              response             The PWAChangeOrderDetailsResponse object returned on request processed. 
 *                                                    Response contains the amazonOrderReferenceId for which the request was processed.
 *                              error                If an error occurred, this error object describes the error. If the
 *                                                    operation completed successfully, this is nil. Incase of error, the response will contain the
 *                                                    amazonOrderReferenceId for which the request was processed.
 */
+ (void)processPayment:(PWAProcessPaymentRequest *)processPaymentRequest
     completionHandler:(void (^)(PWAProcessPaymentResponse *response, NSError *error))completionHandler;


@end
