#import <Foundation/Foundation.h>
#import <PayWithAmazon/PWAWorkflowResponse.h>

__attribute__((deprecated("Use APayChangeOrderDetailsResponse instead.")))
@interface PWAChangeOrderDetailsResponse : NSObject <PWAWorkflowResponse>

@property (copy, nonatomic) NSString *amazonOrderReferenceId;

@end
