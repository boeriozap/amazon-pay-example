#import <Foundation/Foundation.h>

#import "APayPrice.h"
#import "APaySellerOrderAttributes.h"
#import "APayAuthorizeAttributes.h"
#import "APayProviderAttributes.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Request object to process payment.
 */
@interface APayProcessPaymentRequest : NSObject

/**
 * Order reference ID received in createOrderReference response
 */
@property (copy, nonatomic, readonly) NSString *amazonOrderReferenceID;

/**
 * Order amount to be processed
 * The currency code sent will also be used to display amount on payment pages
 */
@property (strong, nonatomic, readonly) APayPrice *orderTotal;

/**
 * Refer to integration guide on how to generate signature for request
 */
@property (copy, nonatomic) NSString *signature;

/**
 * Your public access key ID, named as "Access Key ID" on seller central.
 */
@property (copy, nonatomic, readonly) NSString *awsAccessKeyID;

/**
 * Specifies when funds are authorized and captured
 */
@property (copy, nonatomic, nullable) NSString *paymentAction;

/**
 * The merchant-specified attributes of the order reference
 */
@property (strong, nonatomic, nullable) APaySellerOrderAttributes *sellerOrderAttributes;

/**
 * The merchant-specified attributes to authorize payment
 */
@property (strong, nonatomic, nullable) APayAuthorizeAttributes *authorizeAttributes;

/**
 * Provider related attributes
 */
@property (strong, nonatomic, nullable) APayProviderAttributes *providerAttributes;

- (instancetype)init __attribute__((unavailable("Must use initWithAmazonOrderReferenceID:orderTotal:awsAccessKeyID:signature: \
                                                or initWithAmazonOrderReferenceID:orderTotal:awsAccessKeyID: instead")));

/**
 * Initialize a process payment request object with required parameters.
 *
 * @param amazonOrderReferenceID    Order reference ID received in createOrderReference response.
 * @param orderTotal                Order amount to be processed.
 *                                  The currency code sent will also be used to display amount on payment pages.
 * @param awsAccessKeyID            Your public access key IDs, names as "Access Key ID" on seller central.
 * @param signature                 Refer to integration guide on how to generate signature for request.
 */
- (instancetype)initWithAmazonOrderReferenceID:(NSString *)amazonOrderReferenceID
                                    orderTotal:(APayPrice *)orderTotal
                                awsAccessKeyID:(NSString *)awsAccessKeyID
                                     signature:(NSString *)signature;


/**
 * Initialize a process payment request object with required parameters except signature.
 *
 * @param amazonOrderReferenceID    Order reference ID received in createOrderReference response.
 * @param orderTotal                Order amount to be processed.
 *                                  The currency code sent will also be used to display amount on payment pages.
 * @param awsAccessKeyID            Your public access key IDs, names as "Access Key ID" on seller central.
 */
- (instancetype)initWithAmazonOrderReferenceID:(NSString *)amazonOrderReferenceID
                                    orderTotal:(APayPrice *)orderTotal
                                awsAccessKeyID:(NSString *)awsAccessKeyID;

@end

NS_ASSUME_NONNULL_END
