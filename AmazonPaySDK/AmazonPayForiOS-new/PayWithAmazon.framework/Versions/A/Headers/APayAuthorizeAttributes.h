#import <Foundation/Foundation.h>
#import "APayPrice.h"

/**
 * The merchant-specified attributes to authorize payment.
 */
@interface APayAuthorizeAttributes : NSObject

/**
 * Represents the amount to be authorized
 * Full order amount will be authorized if authorizationAmount is not provided
 */
@property (strong, nonatomic) APayPrice *authorizationAmount;

/**
 * A description for the transaction that is displayed in emails to the buyer.
 */
@property (copy, nonatomic) NSString *sellerAuthorizationNote;

/**
 * The description to be shown on the buyer's payment method statement
 * The soft descriptor sent to the payment processor is: “AMZ* <soft descriptor specified here>”
 * Default: “AMZ*<SELLER_NAME> amzn.com/pmts WA”
 * Maximum: 16 characters
 */
@property (copy, nonatomic) NSString *sellerSoftDescriptor;

@end

