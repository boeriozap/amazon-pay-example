#import <Foundation/Foundation.h>
#import <PayWithAmazon/APayChangeOrderDetailsRequest.h>
#import <PayWithAmazon/APayChangeOrderDetailsResponse.h>
#import <PayWithAmazon/APayCreateOrderReferenceRequest.h>
#import <PayWithAmazon/APayPrice.h>
#import <PayWithAmazon/PWAConstants.h>
#import <PayWithAmazon/PWAError.h>
#import <PayWithAmazon/APayPaymentScope.h>
#import <PayWithAmazon/APayProcessPaymentRequest.h>
#import <PayWithAmazon/APayProcessPaymentResponse.h>
#import <PayWithAmazon/PWARegion.h>
#import <PayWithAmazon/AmazonPayUtil.h>

@interface AmazonPay : NSObject

#define PWA_IOS_SDK_VERSION_STRING @"2.1"

/**
 *  @name Configuration
 */

/**
 *  Sets default region to be used by AmazonPay services
 *
 *  @param region PWARegion indicating your region of operation
 */
+ (void)setDefaultRegion:(PWARegion)region;

/**
 *  Gets default region to be used by AmazonPay services
 *
 *  @return PWARegion indicating your region of operation
 */
+ (PWARegion)getDefaultRegion;

/**
 *  Sets default ledger currency code to be used by AmazonPay services
 *
 *  @param currencyCode NSString indicating your ledger currency code
 */
+ (void)setDefaultLedgerCurrency:(NSString *)currencyCode;

/**
 * Sets default locale to be used by payments pages to display content. If a default locale is not set or if nil locale is passed in,
 * the payment pages will use the preferred language set in the device settings
 *
 * @param locale Locale you want to use as default
 */
+ (void)setDefaultLocale:(NSLocale *)locale;

/**
 * @name Initialize payment for buyer's order
 */

/**
 * Initializes payment by creating an order reference object - a record of key attributes necessary to process the buyer's payment. Some of these
 * attributes will be determined by the buyer as they go through the checkout process (for example, shipping address and payment method) and you will
 * set some of these attributes with information about the order (for example, the total amount).  This API will return the
 * order reference identifier which you have to use for calling AmazonPay backend APIs. Refer AmazonPay Integration Guide to get more
 * details on these APIs
 *
 * @param createOrderReferenceRequest    Request object to create order reference.
 * address for processing your payment
 * @param completionHandler A Completion block to be called once order reference object is created. The block receives the following parameters:
 *              amazonOrderReferenceID     The Identifier for the order reference object created. If an error occured, this is nil.
 *              error    If an error occurred, this error object describes the error. If the operation completed successfully, this is nil.
 */
+ (void)createOrderReference:(APayCreateOrderReferenceRequest *)createOrderReferenceRequest
           completionHandler:(void (^)(NSString *amazonOrderReferenceID, NSError *error))completionHandler;

/**
 * Allows the buyer to change their shipping address for the current transaction by redirecting the buyer to
 * amazon hosted pages where they can select a new address from their address book. Payment methods are linked to addresses, so when the shipping
 * address is changed, the buyer will also be redirected to the change payment page to change their payment method as well.
 *
 * @param changeOrderDetailsRequest  Request object to change order details.
 * @param completionHandler   A Completion block to be called once the change is made. The block receives the following parameters:
 *                              response             The APayChangeOrderDetailsResponse object returned on request processed.
 *                                                    Response contains the amazonOrderReferenceId for which the request was processed.
 *                              error                If an error occurred, this error object describes the error. If the
 *                                                    operation completed successfully, this is nil. Incase of error, the response will contain the
 *                                                    amazonOrderReferenceId for which the request was processed.
 */
+ (void)changeShippingAddress:(APayChangeOrderDetailsRequest *)changeOrderDetailsRequest
            completionHandler:(void (^)(APayChangeOrderDetailsResponse *response, NSError *error))completionHandler;

/**
 * Allows the buyer to change their payment method for the current transaction by redirecting the buyer to
 * amazon hosted pages where they can select a new payment method from their wallet.
 *
 * @param changeOrderDetailsRequest  Request object to change order details.
 * @param completionHandler   A Completion block to be called once the change is made. The block receives the following parameters:
 *                              response             The APayChangeOrderDetailsResponse object returned on request processed.
 *                                                    Response contains the amazonOrderReferenceId for which the request was processed.
 *                              error                If an error occurred, this error object describes the error. If the
 *                                                    operation completed successfully, this is nil. Incase of error, the response will contain the
 *                                                    amazonOrderReferenceId for which the request was processed.
 */
+ (void)changePaymentMethod:(APayChangeOrderDetailsRequest *)changeOrderDetailsRequest
          completionHandler:(void (^)(APayChangeOrderDetailsResponse *response, NSError *error))completionHandler;

/**
 * Allows the seller's 3rd party app to initiate a request to process the buyer's payment.
 *
 * @param processPaymentRequest Request object to process payment.
 * @param completionHandler     A Completion block to be called once processing is complete. The block receives the following parameters:
 *                              response             The APayProcessPaymentResponse object returned on request processed.
 *                                                    Response contains the amazonOrderReferenceId for which the request was processed.
 *                              error                If an error occurred, this error object describes the error. If the
 *                                                    operation completed successfully, this is nil. Incase of error, the response will contain the
 *                                                    amazonOrderReferenceId for which the request was processed.
 */
+ (void)processPayment:(APayProcessPaymentRequest *)processPaymentRequest
     completionHandler:(void (^)(APayProcessPaymentResponse *response, NSError *error))completionHandler;


@end
