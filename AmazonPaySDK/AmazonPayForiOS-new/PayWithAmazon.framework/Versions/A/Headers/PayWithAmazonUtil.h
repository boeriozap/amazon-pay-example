#import <Foundation/Foundation.h>

#import "PWAProcessPaymentRequest.h"

__attribute__((deprecated("Use AmazonPayUtil instead")))
@interface PayWithAmazonUtil : NSObject

/**
 * Allows the merhcant to pass in a process payment request object and generate the data string,
 * which can be passed to the merchant's backend for request signature generation.
 *
 * @param request Request object to process payment
 * @return Data string used for signature generation
 */
+ (NSString *)generateStringToSign:(PWAProcessPaymentRequest *)request;

@end
