#import <Foundation/Foundation.h>
#import "PWAProviderCredit.h"

/**
 * Provider related attributes
 */
__attribute__((deprecated("Use APayProviderAttributes instead.")))
@interface PWAProviderAttributes : NSObject

/**
 * Represents the SellerId of the Solution Provider that developed the platform
 */
@property (copy, nonatomic) NSString *providerID;

/**
 * Solution provider's cut.
 */
@property (copy, nonatomic) NSMutableArray<PWAProviderCredit *> *providerCreditList;

@end
