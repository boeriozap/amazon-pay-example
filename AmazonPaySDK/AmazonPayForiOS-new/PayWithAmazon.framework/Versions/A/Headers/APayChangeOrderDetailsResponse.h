#import <Foundation/Foundation.h>
#import <PayWithAmazon/PWAWorkflowResponse.h>

@interface APayChangeOrderDetailsResponse : NSObject <PWAWorkflowResponse>

@property (copy, nonatomic, readonly) NSString *amazonOrderReferenceId;

@end
