#import <Foundation/Foundation.h>
#import "APayProviderCredit.h"

/**
 * Provider related attributes
 */
@interface APayProviderAttributes : NSObject

/**
 * Represents the SellerId of the Solution Provider that developed the platform
 */
@property (copy, nonatomic) NSString *providerID;

/**
 * Solution provider's cut.
 */
@property (copy, nonatomic) NSMutableArray<APayProviderCredit *> *providerCreditList;

@end
