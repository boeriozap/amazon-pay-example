#import <Foundation/Foundation.h>

/**
 * Request object to change order details.
 */
__attribute__((deprecated("Use APayChangeOrderDetailsRequest instead.")))
@interface PWAChangeOrderDetailsRequest : NSObject

@property (copy, nonatomic, readonly) NSString *amazonOrderReferenceId;

- (instancetype)init __attribute__((unavailable("Must use initWithAmazonOrderReferenceID: instead")));

/**
 * Initialize a change order details request object with required parameters.
 *
 * @param amazonOrderReferenceID    Order reference ID received in createOrderReference response.
 */
- (instancetype)initWithAmazonOrderReferenceID:(NSString *)amazonOrderReferenceID;

@end
