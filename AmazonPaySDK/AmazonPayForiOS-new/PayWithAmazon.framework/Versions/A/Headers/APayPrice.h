#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * The order amount in a specified currency
 */
@interface APayPrice : NSObject

/**
 * The order amount
 */
@property (strong, nonatomic, readonly) NSDecimalNumber *amount;

/**
 * The currency code
 */
@property (copy, nonatomic, readonly) NSString *currencyCode;

- (instancetype)init __attribute__((unavailable("Must use initWithAmount:currencyCode: instead")));

/**
 * Construct a price with the required parameters.
 *
 * @param amount        The order amount.
 * @param currencyCode  The currency code.
 */
+ (instancetype)priceWithAmount:(NSDecimalNumber *)amount currencyCode:(NSString *)currencyCode;

/**
 * Initialize with required parameters.
 *
 * @param amount        The order amount.
 * @param currencyCode  The currency code.
 */
- (instancetype)initWithAmount:(NSDecimalNumber *)amount currencyCode:(NSString *)currencyCode;

@end

NS_ASSUME_NONNULL_END
