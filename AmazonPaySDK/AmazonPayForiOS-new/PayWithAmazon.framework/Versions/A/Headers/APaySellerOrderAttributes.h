#import <Foundation/Foundation.h>

/**
 * The merchant-specified attributes of the order reference
 */
@interface APaySellerOrderAttributes  : NSObject

/**
 * The merchant-specified identifier of this order
 * This is displayed to the buyer in their emails and transaction history on the Amazon Payments website
 */
@property (copy, nonatomic) NSString *sellerOrderID;

/**
 * The identifier of the store from which the order was placed
 * This overrides the default value in Seller Central under Settings > Account Settings
 * It is displayed to the buyer in their emails and transaction history on the Amazon Payments website
 */
@property (copy, nonatomic) NSString *sellerStoreName;

/**
 * Any additional information that merchant would like to include with the order for their reference
 */
@property (copy, nonatomic) NSString *customInformation;

/**
 * A description of an order that is displayed in emails from Amazon to the buyer
 */
@property (copy, nonatomic) NSString *sellerNote;
@end

