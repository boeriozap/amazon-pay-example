#import <Foundation/Foundation.h>

/**
 *  ErrorDomain for LWA errors
 */
extern NSString *const kPWAErrorDomain;

/**
 * Property name used for payment region
 */
extern NSString *const kLPAPaymentRegion;

/**
 * Property name used for locale
 */
extern NSString *const kPWALocale;
