# Pay with Amazon iOS Sample App


## App Design Summary

This is a sample e-commerce app for a fictional company called MTrend. The purpose of this app is to help merchants get a
feel for how Pay with Amazon may look in their mobile app, and to help developers integrate with the Pay with Amazon
Mobile SDK. The app has much of the functionality that is expected from a standard e-commerce app, and all of the Login and Pay
With Amazon API calls necessary during integration.

## Config SDKs

Before you can compile this app, you must copy necessary SDKs to this project.
* LoginWithAmazon. Copy the LoginWithAmazon.framework to PWAiOSSampleApp/LoginWithAmazon folder.
* PayWithAmazon. Copy the PayWithAmazon.framework to PWAiOSSampleApp/PayWithAmazon folder.
* AWS SDKs. Copy the AWSCore.framework, AWSCloudWatch.framework, AWSKinesis.framework and AWSMobileAnalytics.framework to PWAiOSSampleApp/AWSSDKs folder.https://aws.amazon.com/mobile/sdk/

## How to use

Before you can run this app, you must add some seller-specific information.
* In the **plist** file that corresponds to your region (i.e. a DE merchant should use DEMerchant-Info.plist), add your 
**LWA APIKey** to the row labled **APIKey**. You can retrieve this from seller central after you have registered your app. 
Also add your **AWSAccessKeyId** to the row labeled **AWSAccessKeyId**.
* In project settings, click on your desired build target and go to Build Settings. In the Packaging section there is a setting
called **Project Bundle Identifier**, in which you need to insert the bundle identifier that you registered your app with.
* *For demo purposes only* you may wish to use the MockBackend group if a backend server has not been set up yet. To use this
group, insert your **Amazon Seller Id**, **AWS Access Key Id**, and **Secret Access Key** into their respective rows in the
**MockBackend-Info.plist** file. You can retrieve these from seller central. Please note that using the MockBackend group is not
a secure solution to implementing the get order details flow.
* When you run the app, be sure to select the build target associated with your region - US, DE, UK, or JP.

## Login and Pay with Amazon Integration

The following sections detail the different aspects of Login and Pay with Amazon integration and the files that are
used in the integration process. The files that are not mentioned do not contain code necessary for integration.


### Settings

There are a few settings that can be set for Login and Pay with Amazon integration. Note that this app contains a settings
page, which is not something that you will need to implement when integrating with Login and Pay with Amazon.

**Sandbox mode** should be set when you are testing out Login and Pay with Amazon integration. This will allow for
proper testing before going live. The buyer experience will be the same, but no actual transaction will occur. This
is set in **AppDelegate.m**.

**Pay with Amazon default region** is set using the required **setDefaultRegion** PWA API call. This region indicates which
region you are registered with Pay with Amazon. The **setDefaultRegion** call is made in **AppDelegate.m**. The default region 
is found in the **plist** file.

**Pay with Amazon default ledger currency** is set using the required **setDefaultLedgercurrency** PWA API call. This is the
ledger currency in which you want the funds to be disbursed for Pay with Amazon transactions. This API call is made in
**AppDelegate.m**. The default ledger currency is found in the **plist** file.

**Pay with Amazon default locale** is set using the optional **setDefaultLocale** PWA API call. This is the locale used
when displaying translated Login and Pay with Amazon content. If it is not set, the device default is used instead.
The initial default locale is found in the **plist** file. The **PWADemoPickerData.m** file has the list of locales available
to each merchant.

**Login with Amazon region** is set using the **region** property of the shared **AMZNAuthorizationManager**. Setting
this is optional. It is set in **AppDelegate.m** to account for merchants from different regions. The initial LWA region is found
in the **plist** file.


### Login with Amazon

##### Authorize

Login with Amazon occurs through an **authorize:withHandler** LWA API call made by the shared **AMZNAuthorizationManager**.
The code for this call is contained in the **PWADemoLoginWithAmazonUtils.m** file. This file contains a method that builds
a completion handler for the **authorize:withHandler** call and a method that will make the API call. See the header
file, **PWADemoLoginWithAmazonUtils.h**, for more details on the **authorize:withHandler** LWA API call and how to set
it up.

In this app, there are three pages in which Login with Amazon may occur: the login page, the product list page, and the
cart page. See **PWADemoLoginViewController.m**, **PWADemoProductListViewController.m**, and **PWADemoCartViewController.m**
for details.

##### Sign Out
Logout of Amazon occurs through a **signOut** LWA API call made by the shared **AMZNAuthorizationManager**. You should note
that this is not a mandatory step when integrating with Login and Pay with Amazon. The code for this call is also contained
in the **PWADemoLoginWithAmazonUtils.m** file. In the file, there is a method that builds a completion handler for the
**signOut** call and a method that will make the API call. See the header file, **PWADemoLoginWithAmazonUtils.h**, for
more details.

In this app, there are two pages in which Logout of Amazon may occur: the settings page and the product list page. See
**PWADemoSettingsViewController.m** and **PWADemoProductListViewController.m** for details.


### Pay with Amazon

##### Create Order Reference Object

Creating an Amazon order reference object occurs through a **createOrderReference:completionHandler** PWA API call. The code for
this call is contained in the **PWADemoPayWithAmazonUtils.m** file. This file contains a method that builds a completion handler
for the **createOrderReference:completionHandler** call and a method that will make the API call. See the header file,
**PWADemoPayWithAmazonUtils.h**, for more details on the **createOrderReference:completionHandler** PWA API call and how to
set it up.

In this app, there is one page in which creating an order reference object occurs: the cart page. Once the user presses checkout
(or the Pay with Amazon button), an attempt is made to create an order reference object. On success, the app gets the details of
the order and segues to the review order page. See **PWADemoCartViewController.m** for details.

##### Get Order Reference Details

Get order reference details occurs through a call to your backend. The actual PWA API call should be made from your backend because
it involves sensitive information that should not be stored in-app. Sample code for how to set up the call to your backend is contained
in the files **PWADemoPayWithAmazonUtils.m**. Sample code on parsing the details obtained from your backend is contained in the file
**PWADemoXMLParser.m**. This parser is imperfect and will not display all of the information in the response (for example, it will only
tell you one of the constaints). See the header file, **PWADemoPayWithAmazonUtils.h**, for more details on the get order reference
details process.

In this app, there are two pages in which get order reference details occurs: the cart page and the review order page. It occurs in
the cart page after the initial call to create an order reference object. It occurs in the review order page whenever the user changes
their order details. See **PWADemoCartViewController.m** and **PWADemoReviewOrderViewController.m** for details.

##### Change Payment Method

Change payment method occurs through a **changePaymentMethod:completionHandler** PWA API call. The code for this call is contained in 
the **PWADemoPayWithAmazonUtils.m** file. This file contains a method that builds a completion handler for the 
**changePaymentMethod:completionHandler** call and a method that will make the API call. See the header file, **PWADemoPayWithAmazonUtils.h**, 
for more details on the **changePaymentMethod:completionHandler** PWA API call and how to set it up.

In this app, there is one page in which changing payment method occurs: the review order page. Once the user presses the "change" button next
to the payment method details view, the **changePaymentMethod:completionHandler** call will be made via **PWADemoPayWithAmazonUtils**.
Once the payment method has been changed, the view is refreshed by making another call to get order reference details. See
**PWADemoReviewOrderViewController.m** for details.

##### Change Shipping Address

Change shipping address occurs through a **changeShippingAddress:completionHandler** PWA API call. The code for this call is contained in 
the **PWADemoPayWithAmazonUtils.m** file. This file contains a method that builds a completion handler for the 
**changeShippingAddress:completionHandler** call and a method that will make the API call. See the header file, **PWADemoPayWithAmazonUtils.h**, 
for more details on the **changeShippingAddress:completionHandler** PWA API call and how to set it up.

In this app, there is one page in which changing payment method occurs: the review order page. Once the use presses the "change" button next
to the shipping address details view, the **changeShippingAddress:completionHandler** call will be made via **PWADemoPayWithAmazonUtils**.
Once the payment method has been changed, the view is refreshed by making another call to get order reference details. See
**PWADemoReviewOrderViewController.m** for details.

##### Process Payment

Process payment occurs through a **processPayment:completionHandler** PWA API call. The code for this call is contained in 
the **PWADemoPayWithAmazonUtils.m** file. This file contains a method that builds a completion handler for the 
**processPayment:completionHandler** call and a method that will make the API call. See the header file, **PWADemoPayWithAmazonUtils.h**, 
for more details on the **processPayment:completionHandler** PWA API call and how to set it up.

In this app, there is one page in which process payment occurs: the review order page. Once the user presses the "place order" button at the
bottom of the screen, the **processPayment:completionHandler** call will be made via **PWADemoPayWithAmazonUtils**.
If processing the payment fails, the user will remain on the review order page. If process payment succeeds, then the user is redirected to
the thank you page. See **PWADemoReviewOrderViewController.m** for details.
