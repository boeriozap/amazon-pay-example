//
//  PWADemoSeettingsViewController.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoSettingsViewController.h"
#import "PWADemoAmazonAccount.h"
#import "UIViewController+PWADemoAlertMessage.h"
#import "PWADemoLoginWithAmazonUtils.h"
#import "PWADemoPickerData.h"
#import "PWADemoRegionUtils.h"
#import "PWADemoCurrencyFormatter.h"
#import "PWADemoDefaultSettings.h"
#import "PWADemoAlertMessageUtils.h"
#import <LoginWithAmazon/LoginWithAmazon.h>


/**
 *  The PWADemoSettingsViewController controls the Settings scene that can be accessed from the login page.
 *  From here, the user can change the merchant settings.
 */
@interface PWADemoSettingsViewController () <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *localePicker;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UISwitch *sandboxSwitch;

@end

@implementation PWADemoSettingsViewController

#pragma mark - Display

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.activityIndicator setColor:[UIColor grayColor]];
    [self.sandboxSwitch setOn:[AMZNAuthorizationManager sharedManager].sandboxMode];
    [self.localePicker selectRow:[[PWADemoPickerData localeIdentifiers] indexOfObject:[PWADemoDefaultSettings sharedSettings].defaultLocaleIdentifier]
                     inComponent:0
                        animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([PWADemoAlertMessageUtils alertMessageIsSet]) {
        [self raiseAlertMessage:[PWADemoAlertMessageUtils getAndClearAlertMessage]];
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[PWADemoPickerData localeIdentifiers] count];
}

#pragma mark - UIPickerViewDelegate

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    if (!pickerLabel) {
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.font = [UIFont systemFontOfSize:15];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    [pickerLabel setText:[[PWADemoPickerData localeIdentifiers] objectAtIndex:row]];
    
    return pickerLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [[PWADemoDefaultSettings sharedSettings] setDefaultLocaleIdentifier:[[PWADemoPickerData localeIdentifiers] objectAtIndex:row]];
    [AmazonPay setDefaultLocale:[NSLocale localeWithLocaleIdentifier:[PWADemoDefaultSettings sharedSettings].defaultLocaleIdentifier]];
    NSLog(@"Default locale identifier set to %@", [PWADemoDefaultSettings sharedSettings].defaultLocaleIdentifier);
}

- (IBAction)changeSandboxMode:(UISwitch *)sandboxSwitch {
    [AMZNAuthorizationManager sharedManager].sandboxMode = sandboxSwitch.on;
}

#pragma mark - Login with Amazon

- (IBAction)logoutOfAmazon:(id)sender
{
    [self.activityIndicator beginWaitActivity];
    
    PWADemoLogoutHandler completionHandler = [PWADemoLoginWithAmazonUtils buildLogoutHandlerWithHandler:^(NSError *error) {
        [self.activityIndicator endWaitActivity];
    }];
    
    [PWADemoLoginWithAmazonUtils logoutOfAmazonWithCompletionHandler:completionHandler];
}

@end
