//
//  PWADemoReviewOrderViewController.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWADemoOrderReference.h"

@interface PWADemoReviewOrderViewController : UIViewController

@property PWADemoOrderReference *order;

@end
