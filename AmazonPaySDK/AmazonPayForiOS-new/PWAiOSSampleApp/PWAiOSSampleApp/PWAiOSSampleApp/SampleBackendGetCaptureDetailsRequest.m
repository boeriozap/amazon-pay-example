//
//  SampleBackendGetCaptureDetailsRequest.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendGetCaptureDetailsRequest.h"
#import "PWADemoConstants.h"

@implementation SampleBackendGetCaptureDetailsRequest

- (instancetype)init {
    self = [super init];
    if (self) {
        self.path = kGetCaptureDetailsEndpointPath;
    }
    return self;
}

- (instancetype)initWithAmazonCaptureID:(NSString *)amazonCaptureID {
    self = [self init];
    if (self) {
        self.amazonCaptureID = amazonCaptureID;
    }
    
    return self;
}

- (NSDictionary *)requestQueryItems {
    NSMutableDictionary *queryStrings = [NSMutableDictionary dictionary];
    queryStrings[kGetDetailsAmazonCaptureIDParameterName] = self.amazonCaptureID;
    
    return queryStrings;
}

@end
