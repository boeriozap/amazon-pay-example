//
//  PWADemoPayWithAmazonUtils.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import PayWithAmazon;

typedef void (^PWADemoCreateOrderHandler)(NSString *, NSError *);
typedef void (^PWADemoGetDetailsHandler)(NSDictionary *, NSError *);
typedef void (^PWADemoChangeOrderDetailsHandler)(APayChangeOrderDetailsResponse *, NSError *);
typedef void (^PWADemoProcessPaymentHandler)(APayProcessPaymentResponse *, NSError *);
typedef void (^PWADemoProcessPaymentDetailsHandler)(NSDictionary *, NSError *);

/**
 *  The PWADemoPayWithAmazonUtils class contains methods that prepare for and make Pay with Amazon API calls.
 */
@interface PWADemoPayWithAmazonUtils : NSObject

/**
 *  Attempt to create an order reference object. In this method, we initialize a APayCreateOrderReferenceRequest
 *  that will use the Amazon address book because we want the shipping address. We then have the option to
 *  override the default region, ledger currency, and locale if desired. Once our request is ready, we call
 *  the createOrderReference:completionHandler AmazonPay API call using the request and a completion handler as
 *  parameters.  Upon successful order reference creation, the completion handler will be called with a string
 *  set to the Amazon order reference Id. If creation fails, the completion handler is called with the error
 *  set.
 *
 *  @param completionHandler Completion handler to run on create order reference success or failure
 */
+ (void)createOrderReferenceWithCompletionHandler:(PWADemoCreateOrderHandler)completionHandler;

/**
 *  Build a completion handler for use in create order reference object attempt. The completion handler will check
 *  for and log errors. Upon success, the Amazon order reference Id will be logged. After the error check, the
 *  supplementary handler is called. Use this for any customization needed in the completion handler. In this app,
 *  the supplementary handler is used to get the details of the order reference object.
 *
 *  @param supplementaryHandler Supplementary handler to be inserted at end of create order completion handler
 *  @return                     Completion handler for use in create order attempt
 */
+ (PWADemoCreateOrderHandler)buildCreateOrderHandlerWithHandler:(void (^)(NSString *, NSError *))supplementaryHandler;

/**
 *  Attempt to get the order reference details for the order specified by amazonOrerReferenceId. In this method we make an first
 *  make and Authorize LWA API call to get an access token. We then send this token, the Amazon order reference Id, and a completion
 *  handler to the merchant backend which will make the actual getOrderReferenceDetails AmazonPay API call. That call needs to be made in
 *  the backend because it requires sensitive information that should not be stored in-app.
 *
 *  @param amazonOrderReferenceId   The Amazon order reference Id for the order
 *  @param completionHandler        A completion handler to be run on success or failure of getOrderRefernceDetails
 */
+ (void)getOrderReferenceDetailsForOrderId:(NSString *)amazonOrderReferenceID completionHandler:(void (^)(NSDictionary *, NSError *))completionHandler;

/**
 *  Build a completion handler for use in get order reference details attempt. The completion handler will check for and log
 *  errors. On error, the parsedDetailsHandler parameter will be called with a nil dictionary and the error set. Upon success,
 *  The parseData:supplementaryHandler method will be called with the data received from the backend and the parsedDetailsHandler.
 *  This method will parse the data into dictionary filled with order details. The supplementary handler will run once the data 
 *  has been parsed, and is used for any necessary customization.
 *  
 *  @param parsedDetailsHandler Handler to be called upon getOrderReferenceDetails failure, or success (once the data has been parsed)
 *  @return                     Completion handler for use in get order reference details attempt
 */
+ (PWADemoGetDetailsHandler)buildGetDetailsHandlerWithParsedDetailsHandler:(void (^)(NSDictionary *, NSError*))parsedDetailsHandler;

/**
 *  Attempt to change the payment method for the order specified by amazonOrderReferenceId. In this method we first construct a
 *  APayChangeOrderDetailsRequest using the amazonOrderReferenceId. We then make the call to changePaymentMethod:completionHandler using
 *  the request and the completionHandler. Upon success the completion handler will run with a response set. On failure, the
 *  completion handler will run with error set.
 *
 *  @param amazonOrderReferenceId   The Amazon order reference Id for the order
 *  @param completionHandler        A completion handler to be run on success or failure of change payment method attempt
 */
+ (void)changePaymentMethodForOrderId:(NSString *)amazonOrderReferenceId
                    completionHandler:(void (^)(APayChangeOrderDetailsResponse *, NSError *))completionHandler;

/**
 *  Attempt to change the shipping address for the order specified by amazonOrderReferenceId. In this method we first construct a
 *  APayChangeOrderDetailsRequest using the amazonOrderReferenceId. We then make the call to changeShippingAddress:completionHandler using
 *  the request and the completionHandler. Upon success the completion handler will run with a response set. On failure, the
 *  completion handler will run with error set.
 *
 *  @param amazonOrderReferenceId   The Amazon order reference Id for the order
 *  @param completionHandler        A completion handler to be run on success or failure of change shipping address attempt
 */
+ (void)changeShippingAddressForOrderId:(NSString *)amazonOrderReferenceId
                      completionHandler:(void (^)(APayChangeOrderDetailsResponse *, NSError *))completionHandler;

/**
 *  Build a completion handler for use in change order details attempt. The completion handler will check
 *  for and log errors. A successful change will also be logged. After the error check, the supplementary handler is 
 *  called. Use this for any customization needed in the completion handler. In this app, the supplementary handler is 
 *  used to get the details of the order reference object.
 *
 *  @param supplementaryHandler Supplementary handler to be inserted at end of change order details completion handler
 *  @return                     Completion handler for use in change order details
 */
+ (PWADemoChangeOrderDetailsHandler)buildChangeOrderDetailsHandlerWithHandler:(PWADemoChangeOrderDetailsHandler)supplementaryHandler;

/**
 *  Attempt to process payment for the order specified by amazonOrderReferenceId. In this method we first construct a
 *  APayProcessPaymentRequest using the amazonOrderReferenceId. We then set the payment action. We then construst
 *  APaySellerOrderAtributes with various optional details. We then construct the APayAuthorizeAttributes. Once the request is
 *  fully initialized, we generate a signature for our request. You should call your backend to generate this signature. Finally,
 *  the processPayment:completionHandler API call is made with the request and a completion handler. Upon success, the completion
 *  handler runs with the response set. On failure, the completion handler runs with the error set.
 *
 *  @param orderPrice               The price of the order
 *  @param amazonOrderReferenceId   The Amazon order reference Id for the order
 *  @param sellerOrderId            The order Id that is set by the seller
 *  @param completionHandler        A completion handler to be run on success or failure of process payment attempt
 */
+ (void)processPaymentWithPrice:(NSDecimalNumber *)price
         amazonOrderReferenceId:(NSString *)amazonOrderReferenceId
                  sellerOrderId:(NSString *)sellerOrderId
              completionHandler:(PWADemoProcessPaymentHandler)completionHandler;

/**
 *  Build a completion handler for use in process payment attempt. The completion handler will check for and log errors. 
 *  A successful process payment will also be logged. Upon success, a call is made to getOrderReferenceDetails. We make this
 *  call to check to order status and confirm that it is open. To do this, we build a completion handler for our 
 *  getOrderReferenceDetailsWithId:completionHandler call. In this completion handler we check the order status and log if it
 *  is not open. If it is open, then we call the supplementaryDetailsHandler wil the details response set. After the error 
 *  check if-else statement we make a call to the supplementaryHandler for clean up. In this app, we use the
 *  supplementaryDetailsHandler to empty the cart and segue to the thank you page.
 *
 *  @param supplementaryDetailsHandler  Supplementary handler to be inserted at end of successful process payment
 *  @param supplementaryHandler         Supplementary handler to be inserted at end of change order details completion handler
 *  @return                             Completion handler for use in process payment
 */
+ (PWADemoProcessPaymentHandler)buildProcessPaymentHandlerWithDetailsHandler:(PWADemoProcessPaymentDetailsHandler)supplementaryDetailsHandler
                                                        supplementaryHandler:(PWADemoProcessPaymentHandler)supplementaryHandler;

@end
