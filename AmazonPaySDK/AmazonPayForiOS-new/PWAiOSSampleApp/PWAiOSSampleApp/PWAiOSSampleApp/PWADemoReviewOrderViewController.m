//
//  PWADemoReviewOrderViewController.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoReviewOrderViewController.h"
#import "PWADemoCart.h"
#import "PWADemoCurrencyFormatter.h"
#import "PWADemoDateUtils.h"
#import "PWADemoPayWithAmazonUtils.h"
#import "PWADemoAlertMessageUtils.h"
#import "UIViewController+PWADemoAlertMessage.h"
#import "UIActivityIndicatorView+PWADemoActivityIndicator.h"

@interface PWADemoReviewOrderViewController ()

/**
 *  IBOutlets related to order cost
 */
@property (weak, nonatomic) IBOutlet UILabel *merchandisePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCostOfPurchaseLabel;

/**
 *  IBOutlets for displaying payment and shipping info
 */
@property (weak, nonatomic) IBOutlet UILabel *paymentMethodLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingStreetAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingCityStateZipAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedArrivalDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerEmailLabel;

/**
 *  IBOutlets containing payment and shipping info displays
 */
@property (weak, nonatomic) IBOutlet UIView *paymentMethodView;
@property (weak, nonatomic) IBOutlet UIView *shippingAddressView;
@property (weak, nonatomic) IBOutlet UIView *shippingDetailsView;
@property (weak, nonatomic) IBOutlet UIView *contactInfoView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

/**
 *  Set border colors for the payment and shipping info views
 */
- (void)viewDidLoad;

/**
 *  
 */
- (void)viewDidLayoutSubviews;

/**
 *  Displays the price breakdown for the order
 */
- (void)displayPriceBreakdown;

@end

@implementation PWADemoReviewOrderViewController

NSString *const kGoToThankYouPageSegue = @"goToThankYouPage";

#pragma mark - Display

- (void)viewDidAppear:(BOOL)animated
{
    if ([PWADemoAlertMessageUtils alertMessageIsSet]) {
        [self raiseAlertMessage:[PWADemoAlertMessageUtils getAndClearAlertMessage]];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.paymentMethodView.layer.borderColor = [UIColor grayColor].CGColor;
    self.shippingAddressView.layer.borderColor = [UIColor grayColor].CGColor;
    self.shippingDetailsView.layer.borderColor = [UIColor grayColor].CGColor;
    self.contactInfoView.layer.borderColor = [UIColor grayColor].CGColor;
    
    [self updateView];
}

- (void)viewDidLayoutSubviews
{
    [self displayPriceBreakdown];
    
    [self.activityIndicator setColor:[UIColor grayColor]];
}

- (void)displayPriceBreakdown
{
    self.merchandisePriceLabel.text = [PWADemoCurrencyFormatter stringFromNumber:[[PWADemoCart sharedCart] getTotalCost]];
    self.shippingPriceLabel.text = [PWADemoCurrencyFormatter stringFromNumber:[[PWADemoCart sharedCart] getShippingCost]];
    self.taxPriceLabel.text = [PWADemoCurrencyFormatter stringFromNumber:[[PWADemoCart sharedCart] getTaxAmount]];
    self.totalCostOfPurchaseLabel.text = [PWADemoCurrencyFormatter stringFromNumber:[[PWADemoCart sharedCart] getTotalCostWithShippingAndTax]];
}

- (void)updateView
{
    self.paymentMethodLabel.text = [NSString stringWithFormat:@"(%@)", self.order.paymentInstrument];
    self.shippingStreetAddressLabel.text = self.order.shippingAddressStreet;
    self.shippingCityStateZipAddressLabel.text = self.order.shippingAddressGeneral;
    self.expectedArrivalDateLabel.text = [NSString stringWithFormat:@"Expected arrival: %@", [PWADemoDateUtils getNextBusinessWeekDates]];
    self.customerNameLabel.text = self.order.buyerName;
    self.customerEmailLabel.text = self.order.buyerEmail;
}

#pragma mark - Pay With Amazon

- (IBAction)changePaymentMethod:(id)sender
{
    [self.activityIndicator beginWaitActivity];
    PWADemoChangeOrderDetailsHandler completionHandler = [PWADemoPayWithAmazonUtils buildChangeOrderDetailsHandlerWithHandler:[self getChangeOrderDetailsSupplementaryHandler]];
    
    [PWADemoPayWithAmazonUtils changePaymentMethodForOrderId:self.order.amazonOrderReferenceId
                                           completionHandler:completionHandler];
}

- (IBAction)changeShippingAddress:(id)sender
{
    [self.activityIndicator beginWaitActivity];
    PWADemoChangeOrderDetailsHandler completionHandler = [PWADemoPayWithAmazonUtils buildChangeOrderDetailsHandlerWithHandler:[self getChangeOrderDetailsSupplementaryHandler]];
    
    [PWADemoPayWithAmazonUtils changeShippingAddressForOrderId:self.order.amazonOrderReferenceId
                                             completionHandler:completionHandler];
}

- (PWADemoChangeOrderDetailsHandler)getChangeOrderDetailsSupplementaryHandler
{
    return ^(APayChangeOrderDetailsResponse *response, NSError *error) {
        if (!error) {
            [self refreshOrderDetails];
        } else {
            [self.activityIndicator endWaitActivity];
        }
    };
}

- (void)refreshOrderDetails
{
    PWADemoGetDetailsHandler getOrderDetailsHandler = [PWADemoPayWithAmazonUtils buildGetDetailsHandlerWithParsedDetailsHandler: ^(NSDictionary *orderDetails, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                [self.order setOrderDetailsWithDictionary:orderDetails];
            }
            
            [self.activityIndicator endWaitActivity];
            [self viewDidAppear:NO];
            [self updateView];
        });
    }];
    
    [PWADemoPayWithAmazonUtils getOrderReferenceDetailsForOrderId:self.order.amazonOrderReferenceId
                                                completionHandler:getOrderDetailsHandler];
}

- (IBAction)placeOrder:(id)sender
{
    [self.activityIndicator beginWaitActivity];
    
    PWADemoProcessPaymentHandler completionHandler = [PWADemoPayWithAmazonUtils buildProcessPaymentHandlerWithDetailsHandler:^(NSDictionary *details, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[PWADemoCart sharedCart] emptyCart];
            [self performSegueWithIdentifier:kGoToThankYouPageSegue sender:self];
        });
    } supplementaryHandler:^(APayProcessPaymentResponse *response, NSError *error) {
        
        [self.activityIndicator endWaitActivity];
        //[self viewDidAppear:NO];
    }];
    
    [PWADemoPayWithAmazonUtils processPaymentWithPrice:[[PWADemoCart sharedCart] getTotalCostWithShippingAndTax]
                                amazonOrderReferenceId:self.order.amazonOrderReferenceId
                                         sellerOrderId:self.order.sellerOrderId
                                     completionHandler:completionHandler];
}

@end
