//
//  SampleBackendGenerateSignatureRequest.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SampleBackendPostRequest.h"

@interface SampleBackendGenerateSignatureRequest : SampleBackendPostRequest

- (instancetype)initWithTextToSign: (NSString *)textToSign;

@property (nonatomic, copy) NSString *textToSign;

@end
