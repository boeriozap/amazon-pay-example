//
//  PWADemoAmazonIntegrationUtils.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoAlertMessageUtils.h"

@implementation PWADemoAlertMessageUtils

static NSString *alertMessage = nil;

+ (BOOL)alertMessageIsSet
{
    return alertMessage != nil;
}

+ (NSString *)getAndClearAlertMessage
{
    // Save copy of alert message
    NSString *returnMessage = [NSString stringWithString:alertMessage];
    // Clear alert message
    alertMessage = nil;
    return returnMessage;
}

+ (void)setAlertMessage:(NSString *)message
{
    if (alertMessage) {
        NSLog(@"Alert message changed without being retrieved: %@", alertMessage);
    }
    alertMessage = [NSString stringWithString:message];
}

@end
