//
//  PWADemoCurrencyFormatter.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWADemoCurrencyFormatter : NSObject

/**
 *  Returns a string containing the formatted value of the provided number object
 */
+ (NSString *)stringFromNumber:(NSNumber *)number;

/**
 *  Sets the locale identifier associated with the currency format of the given currency
 *  @param ledgerCurrency A ledger currency supported by Pay with Amazon
 */
+ (void)setLocaleIdentifierWithCurrency:(NSString *)ledgerCurrency;

@end
