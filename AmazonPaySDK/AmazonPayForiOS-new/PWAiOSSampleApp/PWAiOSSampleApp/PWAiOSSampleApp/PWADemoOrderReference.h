//
//  PWADemoOrderReference.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  The PWADemoOrderReference class holds information relating to an
 *  Amazon order reference object.
 */
@interface PWADemoOrderReference : NSObject

@property (readonly) NSString *shippingAddressStreet;
@property (readonly) NSString *shippingAddressGeneral;
@property (readonly) NSString *paymentInstrument;
@property (readonly) NSString *buyerEmail;
@property (readonly) NSString *buyerName;
@property (readonly) NSString *amazonOrderReferenceId;
@property (readonly) NSString *sellerOrderId;
@property (readonly) NSString *authorizationId;

/**
 *  Initializes an order reference by assigning it the given Amazon order
 *  reference id and setting an arbitrary seller order id
 *  @param amazonOrderReferenceId   The amazon order reference id for this order
 *  @return                         The initialized order
 */
- (instancetype)initWithAmazonOrderReferenceId:(NSString *)amazonOrderReferenceId;

/**
 *  Uses a dictionary to set the details for the order reference. If certain
 *  important keys are missing from the dictionary, then alert messages are set.
 *  @param detailsDictionary The dictionary with the order's details
 */
- (void)setOrderDetailsWithDictionary:(NSDictionary *)detailsDictionary;

@end
