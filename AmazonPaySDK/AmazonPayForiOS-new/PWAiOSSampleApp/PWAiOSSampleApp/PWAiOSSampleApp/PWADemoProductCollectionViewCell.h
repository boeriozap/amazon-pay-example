//
//  PWADemoProductCollectionViewCell.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWADemoProduct.h"

@interface PWADemoProductCollectionViewCell : UICollectionViewCell

/**
 *  The product displayed in the cell
 */
@property (readonly) PWADemoProduct *product;

/**
 *  The button that is pressed to select a product
 */
@property UIButton *productSelectButton;

/**
 *  Displays product information in the cell
 */
- (void)setDisplayWithProduct:(PWADemoProduct *)product;

@end
