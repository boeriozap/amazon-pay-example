//
//  SampleBackendErrorFactory.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SampleBackendError.h"

@interface SampleBackendErrorFactory : NSObject

/**
 *  Creates NSError for sample backend errors
 *  @param error        SampleBackendError enum indicating the error code
 *  @param description  Error description indicating root cause of the issue
 *  @return             NSError object for the error
 */
+ (NSError *)errorWithSampleBackendError:(SampleBackendError)error description:(NSString *)description;

@end
