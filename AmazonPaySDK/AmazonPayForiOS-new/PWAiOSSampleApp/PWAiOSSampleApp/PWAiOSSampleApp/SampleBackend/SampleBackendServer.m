//
//  SampleBackendServer.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendServer.h"
#import "SampleBackendSignatureUtils.h"
#import "SampleBackendSellerSettings.h"
#import "SampleBackendServiceClient.h"
#import "SampleBackendErrorFactory.h"
#import "SampleBackendGetOrderReferenceDetailsRequest.h"

@implementation SampleBackendServer

+ (void)getOrderReferenceDetailsWithToken:(NSString *)accessToken
                         orderReferenceID:(NSString *)amazonOrderRefernceID
                        completionHandler:(void (^)(NSDictionary *jsonDictionary, NSError *error))completionHandler
{
    SampleBackendGetOrderReferenceDetailsRequest *request = [[SampleBackendGetOrderReferenceDetailsRequest alloc] initWithAmazonOrderReferenceID:amazonOrderRefernceID accessToken:accessToken];
    
    [request sendRequestWithCompletionHandler:^(NSDictionary *jsonDictionary, NSError *error) {
        completionHandler(jsonDictionary, nil);
    }];
}

+ (NSString *)generateSignatureForProcessPaymentRequest:(APayProcessPaymentRequest *)request
{
    NSError *error;
    return [SampleBackendSignatureUtils signProcessPaymentRequest:&request
                                                secretAccesskey:[SampleBackendSellerSettings sharedSettings].secretAccessKey
                                                          error:&error];
}
 
@end
