//
//  SampleBackendError.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Enum values for errors from sample backend
 */
typedef NS_ENUM(NSInteger, SampleBackendError)
{
    SAMPLE_APP_NETWORK_UNAVAILABLE,
    SAMPLE_APP_SYSTEM_ERROR,
    SAMPLE_APP_INVALID_RESPONSE
};
