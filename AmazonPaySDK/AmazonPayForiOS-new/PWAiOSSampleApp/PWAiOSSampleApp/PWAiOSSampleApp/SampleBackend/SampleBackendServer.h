//
//  SampleBackendServer.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
@import PayWithAmazon;

/**
 *  The SampleBackendServer class is a placeholder for a real backend server. It has the API that the real
 *  backend should have and calls upon a variety of classes, all of which should be in the backend.
 */
@interface SampleBackendServer : NSObject

+ (void)getOrderReferenceDetailsWithToken:(NSString *)accessToken
                         orderReferenceID:(NSString *)amazonOrderRefernceID
                        completionHandler:(void (^)(NSDictionary *getOrderReferenceDetailsWithToken, NSError *error))completionHandler;

+ (NSString *)generateSignatureForProcessPaymentRequest:(APayProcessPaymentRequest *)request;

@end
