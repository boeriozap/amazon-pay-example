//
//  SampleBackendServiceClient.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SampleBackendNetworkReachability.h"

@interface SampleBackendServiceClient : NSObject<NSURLSessionDelegate>

- (instancetype)initWithURLSession:(NSURLSession *)session withReachability:(SampleBackendNetworkReachability *)reachability;

- (void)executeRequest:(NSURLRequest *)request onRequestCompletion:(void (^)(NSData *data, NSError *urlError))onRequestCompletion;

@end
