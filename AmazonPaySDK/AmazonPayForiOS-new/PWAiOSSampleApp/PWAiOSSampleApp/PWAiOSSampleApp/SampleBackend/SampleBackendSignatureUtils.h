//
//  SampleBackendSignatureUtils.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
@import PayWithAmazon;

@interface SampleBackendSignatureUtils : NSObject

+ (NSString *)signProcessPaymentRequest:(APayProcessPaymentRequest *__autoreleasing *)request
                        secretAccesskey:(NSString *)secretAccesskey
                                  error:(NSError *__autoreleasing *)error;

@end
