//
//  SampleBackendSellerSettings.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SampleBackendSellerSettings : NSObject

@property (readonly) NSString *sellerID;
@property (readonly) NSString *awsAccessKetID;
@property (readonly) NSString *secretAccessKey;
@property (readonly) NSString *backendServerHost;

+ (instancetype)sharedSettings;

@end
