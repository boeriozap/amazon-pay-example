//
//  SampleBackendSellerSettings.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendSellerSettings.h"

@implementation SampleBackendSellerSettings

#if US_MERCHANT
NSString *const kMerchant = @"USMerchant";
#elif DE_MERCHANT
NSString *const kMerchant = @"DEMerchant";
#elif UK_MERCHANT
NSString *const kMerchant = @"UKMerchant";
#elif JP_MERCHANT
NSString *const kMerchant = @"JPMerchant";
#endif

+ (instancetype)sharedSettings
{
    static SampleBackendSellerSettings *sharedSettings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSettings = [[SampleBackendSellerSettings alloc] init];
    });
    return sharedSettings;
}

- (instancetype)init
{
    NSString *plistFilePath = [[NSBundle mainBundle] pathForResource:@"SampleBackend-Info" ofType:@"plist"];
    NSDictionary *SampleBackendPlist = [[NSDictionary dictionaryWithContentsOfFile:plistFilePath] objectForKey:kMerchant];
    _sellerID = [SampleBackendPlist objectForKey:@"SellerID"];
    _awsAccessKetID = [SampleBackendPlist objectForKey:@"AWSAccessKeyId"];
    _secretAccessKey = [SampleBackendPlist objectForKey:@"SecretAccessKey"];
    _backendServerHost = [SampleBackendPlist objectForKey:@"BackendServerHost"];
    NSLog(@"Sample backend setting Seller ID: %@", self.sellerID);
    NSLog(@"Sample backend setting AWS Access Key ID: %@", self.awsAccessKetID);
    NSLog(@"Sample backend setting Secret Access Key: %@", self.secretAccessKey);
    NSLog(@"Sample backend setting Backend Server Host: %@", self.backendServerHost);
    
    return self;
}

@end
