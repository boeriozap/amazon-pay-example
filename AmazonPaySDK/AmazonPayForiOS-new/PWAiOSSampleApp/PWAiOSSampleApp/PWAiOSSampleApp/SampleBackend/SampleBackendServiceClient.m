//
//  SampleBackendServiceClient.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendServiceClient.h"
#import "SampleBackendErrorFactory.h"
#import "SampleBackendError.h"

typedef void (^NSURLSessionDataTaskHandler)(NSData *, NSURLResponse *, NSError *);

static int MAXIMUM_RETRY_COUNT = 3;

static int HTTP_SUCCESS_CODE_START = 200;
static int HTTP_SUCCESS_CODE_END = 300;

static int HTTP_UNRECOVERABLE_ERROR_CODE_START = 400;
static int HTTP_UNRECOVERABLE_ERROR_CODE_END = 500;

static int HTTP_RECOVERABLE_ERROR_CODE_START = 500;
static int HTTP_RECOVERABLE_ERROR_CODE_END = 600;

typedef NS_ENUM(NSUInteger, SampleBackendOperationStatus)
{
    SUCCESS,
    RETRYABLE,
    NONRETRYABLE,
    FAILED,
};

@interface SampleBackendServiceClient ()

@property (strong, nonatomic) NSURLSession *urlSession;
@property (strong, nonatomic) SampleBackendNetworkReachability *reachability;

@end

@implementation SampleBackendServiceClient

- (instancetype)init
{
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    config.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
    SampleBackendNetworkReachability *reachability = [[SampleBackendNetworkReachability alloc] init];
    return [self initWithURLSession:[NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil] withReachability:reachability];
}

- (instancetype)initWithURLSession:(NSURLSession *)session withReachability:(SampleBackendNetworkReachability *)reachability
{
    self = [super init];
    if (self) {
        self.urlSession = session;
        self.reachability = reachability;
    }
    return self;
}

- (void)executeRequest:(NSURLRequest *)request onRequestCompletion:(void (^)(NSData *data, NSError *urlError))onRequestCompletion
{
    [self dispatchAsync:request onRequestCompletion:onRequestCompletion withRetriesLeft:MAXIMUM_RETRY_COUNT];
}

- (void)dispatchAsync:(NSURLRequest *)request
  onRequestCompletion:(void (^)(NSData *data, NSError *urlError))onRequestCompletion
      withRetriesLeft:(NSUInteger)retries
{
    if (![self.reachability isNetworkAvailableForURL:[request URL]]) {
        onRequestCompletion(nil, [SampleBackendErrorFactory errorWithSampleBackendError:SAMPLE_APP_NETWORK_UNAVAILABLE
                                                                        description:@"Could not connect to network. Please check your network connection"]);
        return;
    }
    if (retries == 0) {
        onRequestCompletion(nil, [SampleBackendErrorFactory errorWithSampleBackendError:SAMPLE_APP_SYSTEM_ERROR
                                                                        description:@"Pay With Amazon Service is unavailable. Please contact Amazon to resolve issue"]);
        return;
    }
    retries--;
    
    NSURLSessionDataTaskHandler handler = ^(NSData *data, NSURLResponse *response, NSError *urlError) {
        if (urlError) {
            onRequestCompletion(data, [SampleBackendErrorFactory errorWithSampleBackendError:SAMPLE_APP_SYSTEM_ERROR description:urlError.description]);
            return;
        } else if (!response) {
            NSString *description = [NSString stringWithFormat:@"Received nil response while executing request for URL %@", [[request URL] absoluteString]];
            onRequestCompletion(data, [SampleBackendErrorFactory errorWithSampleBackendError:SAMPLE_APP_SYSTEM_ERROR description:description]);
            return;
        }
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger statusCode = [httpResponse statusCode];
        SampleBackendOperationStatus status = [self getOperationStatusForCode:statusCode];
        
        switch (status) {
                
            case SUCCESS:
                onRequestCompletion(data, urlError);
                break;
                
            case RETRYABLE:
                [self dispatchAsync:request onRequestCompletion:onRequestCompletion withRetriesLeft:retries];
                break;
                
            case NONRETRYABLE:
                onRequestCompletion(data, [SampleBackendErrorFactory errorWithSampleBackendError:SAMPLE_APP_SYSTEM_ERROR
                                                                                 description:[NSHTTPURLResponse localizedStringForStatusCode:statusCode]]);
                break;
                
            default:
                onRequestCompletion(data, [SampleBackendErrorFactory errorWithSampleBackendError:SAMPLE_APP_SYSTEM_ERROR
                                                                                 description:[NSHTTPURLResponse localizedStringForStatusCode:statusCode]]);
                break;
        }
        
    };
    NSURLSessionDataTask *dataTask = [self.urlSession dataTaskWithRequest:request completionHandler:handler];
    
    [dataTask resume];
}

- (SampleBackendOperationStatus)getOperationStatusForCode:(NSInteger)statusCode
{
    if (statusCode >= HTTP_SUCCESS_CODE_START && statusCode < HTTP_SUCCESS_CODE_END) {
        return SUCCESS;
    } else if (statusCode >= HTTP_RECOVERABLE_ERROR_CODE_START && statusCode < HTTP_RECOVERABLE_ERROR_CODE_END) {
        return RETRYABLE;
    } else if (statusCode >= HTTP_UNRECOVERABLE_ERROR_CODE_START && statusCode < HTTP_UNRECOVERABLE_ERROR_CODE_END) {
        return NONRETRYABLE;
    }
    return FAILED;
}

- (void)dealloc
{
    [self.urlSession invalidateAndCancel];
}

@end
