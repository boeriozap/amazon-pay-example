//
//  SampleBackendSignatureUtils.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendSignatureUtils.h"
#import "SampleBackendURLUtils.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation SampleBackendSignatureUtils

NSString * const kSignatureAWSAccessKeyIDKey = @"AWSAccessKeyId";
NSString * const kSignatureTimestampKey = @"Timestamp";

+ (NSString *)signProcessPaymentRequest:(APayProcessPaymentRequest *__autoreleasing *)request
                        secretAccesskey:(NSString *)secretAccesskey
                                  error:(NSError *__autoreleasing *)error
{
    if (!(*request)) {
        NSLog(@"Request can not be nil.");
    }
    NSString *awsAccessKeyId = (*request).awsAccessKeyID;
    if (!awsAccessKeyId || [awsAccessKeyId length] == 0) {
        NSLog(@"AWS access key id can not be nil or blank.");
    }
    if (!secretAccesskey || [secretAccesskey length] == 0) {
        NSLog(@"Secret access key can not be nil or blank.");
    }
    
    NSString *dataString = [AmazonPayUtil generateStringToSign:*request];
    
    // TODO Call backend server to generate signature.
    NSString *signature = nil;
    NSLog(@"String to be signed:\n%@", dataString);
    NSLog(@"Signature for process payment:\n%@", signature);
    
    return signature;
}

@end
