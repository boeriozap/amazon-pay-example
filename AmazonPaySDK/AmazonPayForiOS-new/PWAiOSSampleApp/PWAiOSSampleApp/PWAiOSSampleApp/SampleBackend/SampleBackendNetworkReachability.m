//
//  SampleBackendNetworkReachability.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendNetworkReachability.h"

@implementation SampleBackendNetworkReachability

- (BOOL)isNetworkAvailableForURL:(NSURL *)url
{
    if (!url) {
        [NSException raise:NSInvalidArgumentException format:@"Did not receive a URL to check for network connectivity"];
    }
    
    CFNetDiagnosticRef dReference = CFNetDiagnosticCreateWithURL(kCFAllocatorDefault, (__bridge CFURLRef)url);
    CFNetDiagnosticStatus status = CFNetDiagnosticCopyNetworkStatusPassively(dReference, NULL);
    CFRelease(dReference);
    if (status == kCFNetDiagnosticConnectionUp) {
        NSLog(@"Network connection is available");
        return YES;
    } else {
        NSLog(@"Network connection is down");
        return NO;
    }
}

@end
