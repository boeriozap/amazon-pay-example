//
//  SampleBackendURLUtils.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendURLUtils.h"

@implementation SampleBackendURLUtils

+ (NSURL *)buildURLForHost:(NSString *)host path:(NSString *)path URLParams:(NSDictionary<NSString *, NSString *> *)URLParams
{
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:host];
    components.path = path;
    
    if (URLParams) {
        NSMutableArray *queryStringItems = [NSMutableArray array];
        for (NSString *key in URLParams.allKeys) {
            NSURLQueryItem *queryStringItem = [NSURLQueryItem queryItemWithName:key value:URLParams[key]];
            [queryStringItems addObject:queryStringItem];
        }
        
        components.queryItems = queryStringItems;
    }
    
    return components.URL;
}

@end
