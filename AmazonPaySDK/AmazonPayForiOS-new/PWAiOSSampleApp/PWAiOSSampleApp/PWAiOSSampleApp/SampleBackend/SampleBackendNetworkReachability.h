//
//  SampleBackendNetworkReachability.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SampleBackendNetworkReachability : NSObject

- (BOOL)isNetworkAvailableForURL:(NSURL *)url;

@end
