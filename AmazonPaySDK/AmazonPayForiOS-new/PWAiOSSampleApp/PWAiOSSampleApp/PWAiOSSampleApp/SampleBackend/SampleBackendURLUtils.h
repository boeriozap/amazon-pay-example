//
//  SampleBackendURLUtils.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SampleBackendURLUtils : NSObject

+ (NSURL *)buildURLForHost:(NSString *)host path:(NSString *)path URLParams:(NSDictionary<NSString *, NSString *> *)URLParams;

@end
