//
//  SampleBackendErrorFactory.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendErrorFactory.h"

@implementation SampleBackendErrorFactory

+ (NSError *)errorWithSampleBackendError:(SampleBackendError)error description:(NSString *)description
{
    return [SampleBackendErrorFactory createErrorWithDomain:@"Sample Backend" code:error description:description];
}

+ (NSError *)createErrorWithDomain:(NSString *)domain code:(NSInteger)code description:(NSString *)description
{
    NSDictionary *userInfoDict = @{ NSLocalizedDescriptionKey : description };
    return [NSError errorWithDomain:domain code:code userInfo:userInfoDict];
}

@end
