//
//  SampleBackendRequest.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendRequest.h"
#import "SampleBackendSellerSettings.h"
#import "SampleBackendURLUtils.h"
#import "SampleBackendServiceClient.h"
#import "SampleBackendErrorFactory.h"

@implementation SampleBackendRequest

NSString *const kBackendResponseSuccessKey = @"success";
NSString *const kBackendResponseDataKey = @"data";
NSString *const kBackendResponseErrorMessageKey = @"errorMessage";

- (instancetype) init {
    self = [super init];
    if (self) {
        self.host = [SampleBackendSellerSettings sharedSettings].backendServerHost;
    }
    
    return self;
}

- (NSURLRequest *)generateRequest
{
    @throw [SampleBackendErrorFactory errorWithSampleBackendError:SAMPLE_APP_SYSTEM_ERROR description:@"generateRequest not implemented"];
}

- (void) sendRequestWithCompletionHandler:(void (^)(NSDictionary *jsonDictionary, NSError *error))completionHandler {
    SampleBackendServiceClient *serviceClient = [[SampleBackendServiceClient alloc] init];
    [serviceClient executeRequest:[self generateRequest] onRequestCompletion:^(NSData *data, NSError *urlError) {
        if (urlError != nil) {
            completionHandler(nil, urlError);
            return;
        }
        
        NSError *parserError = nil;
        NSDictionary *jsonObject=[NSJSONSerialization
                                  JSONObjectWithData:data
                                  options:kNilOptions
                                  error:&parserError];
        
        if (parserError != nil) {
            completionHandler(nil, parserError);
            return;
        }
        
        NSError *validatationError = nil;
        if (![self isResponseValid:jsonObject error:&validatationError]) {
            completionHandler(nil, validatationError);
        }
        
        completionHandler(jsonObject[kBackendResponseDataKey], nil);
    }];
}

- (BOOL) isResponseValid:(NSDictionary *)response error:(NSError **)error {
    if (response == nil || response[kBackendResponseSuccessKey] == nil || (response[kBackendResponseSuccessKey] && response[kBackendResponseDataKey] ==nil)) {
        *error = [SampleBackendErrorFactory errorWithSampleBackendError:SAMPLE_APP_SYSTEM_ERROR description:@"Invalid service response"];
        return false;
    }
    
    if (!response[kBackendResponseSuccessKey]) {
        *error = [SampleBackendErrorFactory errorWithSampleBackendError:SAMPLE_APP_SYSTEM_ERROR description:response[kBackendResponseErrorMessageKey]];
        return false;
    }
    
    return true;
}

- (NSDictionary *)requestQueryItems {
    // Querystring is optional.
    return nil;
}

@end
