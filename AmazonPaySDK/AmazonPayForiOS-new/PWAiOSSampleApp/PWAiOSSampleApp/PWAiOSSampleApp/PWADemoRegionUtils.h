//
//  PWADemoRegionUtils.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <LoginWithAmazon/LoginWithAmazon.h>
@import PayWithAmazon;

@interface PWADemoRegionUtils : NSObject

+ (PWARegion)pwaRegionFromString:(NSString *)regionString;

+ (NSString *)stringFromPWARegion:(PWARegion)region;

+ (AMZNRegion)lwaRegionFromString:(NSString *)regionString;

+ (NSString *)stringFromLWARegion:(AMZNRegion)region;

@end
