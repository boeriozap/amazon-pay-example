//
//  UIViewController+PWADemoAlertMessage.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 *  The PWADemoAlertMessage category of UIViewController provides a method to raise an alert message
 */
@interface UIViewController (PWADemoAlertMessage)

/**
 *  Initializes and displays an alert controller with the given message and an OK button
 */
- (void)raiseAlertMessage:(NSString *)alertMessage;

@end