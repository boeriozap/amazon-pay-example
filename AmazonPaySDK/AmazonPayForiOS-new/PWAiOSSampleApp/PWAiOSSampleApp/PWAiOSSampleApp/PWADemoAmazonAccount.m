//
//  PWADemoAmazonAccount.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoAmazonAccount.h"

@implementation PWADemoAmazonAccount

+ (instancetype)sharedInstance
{
    static PWADemoAmazonAccount *singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[self alloc] init];
    });
    return singleton;
}

@end
