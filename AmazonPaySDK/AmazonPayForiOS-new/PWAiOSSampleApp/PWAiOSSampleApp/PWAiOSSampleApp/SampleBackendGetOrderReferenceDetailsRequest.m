//
//  SampleBackendGetOrderReferenceDetailsRequest.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendGetOrderReferenceDetailsRequest.h"
#import "PWADemoConstants.h"

@implementation SampleBackendGetOrderReferenceDetailsRequest

NSString *const kGetOrderReferenceDetailsResultKey = @"getOrderReferenceDetailsResult";
NSString *const kOrderReferenceDetailsKey = @"orderReferenceDetails";
NSString *const kOrderReferenceStatusKey = @"orderReferenceStatus";
NSString *const kStateKey = @"state";

- (instancetype)init {
    self = [super init];
    if (self) {
        self.path = kGetOrderReferenceDetailsEndpointPath;
    }
    return self;
}

- (instancetype)initWithAmazonOrderReferenceID:(NSString *)amazonOrderReferenceID accessToken:(NSString *)accessToken
{
    self = [self init];
    if (self) {
        self.amazonOrderReferenceID = amazonOrderReferenceID;
        self.accessToken = accessToken;
    }
    
    return self;
}

- (NSDictionary *)requestQueryItems {
    NSMutableDictionary *queryStrings = [NSMutableDictionary dictionary];
    queryStrings[kGetDetailsAmazonOrderReferenceIDParameterName] = self.amazonOrderReferenceID;
    queryStrings[kGetDetailsAmazonAccessTokenParameterName] = self.accessToken;
    
    return queryStrings;
}

@end
