//
//  SampleBackendGetAuthorizationDetailsRequest.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SampleBackendGetRequest.h"

@interface SampleBackendGetAuthorizationDetailsRequest : SampleBackendGetRequest

@property (nonatomic, copy) NSString *amazonAuthorizationID;

- (instancetype)initWithAmazonAuthorizationID:(NSString *)amazonAuthorizationID;

@end
