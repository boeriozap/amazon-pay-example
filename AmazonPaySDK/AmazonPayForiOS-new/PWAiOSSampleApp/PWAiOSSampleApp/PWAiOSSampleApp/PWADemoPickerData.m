//
//  PWADemoPickerData.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoPickerData.h"

@implementation PWADemoPickerData

static NSArray<NSString *> *localeIdentifiers;

#pragma mark - Public

+ (NSArray<NSString *> *)localeIdentifiers
{
    return localeIdentifiers;
}

#pragma mark - Private

+ (void)initialize
{
    if (self == [PWADemoPickerData class]) {
        // initialize the available locale identifiers
        localeIdentifiers = @[
#if US_MERCHANT
            @"en_US"
#elif EU_MERCHANT
            @"en_GB", @"de_DE", @"fr_FR", @"it_IT", @"es_ES"
#elif JP_MERCHANT
            @"ja_JP"
#endif
        ];
    }
}

@end
