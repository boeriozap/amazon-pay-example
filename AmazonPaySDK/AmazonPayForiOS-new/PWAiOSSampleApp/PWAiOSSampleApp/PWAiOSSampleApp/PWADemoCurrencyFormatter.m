//
//  PWADemoCurrencyFormatter.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoCurrencyFormatter.h"

@implementation PWADemoCurrencyFormatter

NSString *const kUnitedStatesDollar = @"USD";
NSString *const kGreatBritishPound = @"GBP";
NSString *const kEuro = @"EUR";
NSString *const kJapaneseYen = @"JPY";
NSString *const kUSDLocaleIdentifier = @"en_US";
NSString *const kGBPLocaleIdentifier = @"en_GB";
NSString *const kEURLocaleIdentifier = @"de_DE";
NSString *const kJPYLocaleIdentifier = @"ja_JP";

static NSLocale *localeIdentifier;

+ (NSString *)stringFromNumber:(NSNumber *)number
{
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setLocale:localeIdentifier];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [currencyFormatter stringFromNumber:number];
}

+ (void)setLocaleIdentifierWithCurrency:(NSString *)ledgerCurrency
{
    localeIdentifier = [NSLocale localeWithLocaleIdentifier:[PWADemoCurrencyFormatter getLocaleIdentifierFromCurrency:ledgerCurrency]];
}

+ (NSString *)getLocaleIdentifierFromCurrency:(NSString *)ledgerCurrency
{
    if ([ledgerCurrency isEqualToString:kUnitedStatesDollar]) {
        return kUSDLocaleIdentifier;
    } else if ([ledgerCurrency isEqualToString:kEuro]) {
        return kEURLocaleIdentifier;
    } else if ([ledgerCurrency isEqualToString:kGreatBritishPound]) {
        return kGBPLocaleIdentifier;
    } else if ([ledgerCurrency isEqualToString:kJapaneseYen]) {
        return kJPYLocaleIdentifier;
    } else {
        [NSException raise:NSInvalidArgumentException format:@"Ledger currency not a valid option: %@", ledgerCurrency];
    }
    return nil;
}

@end
