//
//  PWADemoProductTableViewCell.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWADemoCartItem.h"

@interface PWADemoProductTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *decrementQuantityButton;
@property (weak, nonatomic) IBOutlet UIButton *incrementQuantityButton;

/**
 *  Displays cart item information in the cell
 */
- (void)setDisplayWithCartItem:(PWADemoCartItem *)cartItem;

@end
