//
//  PWADemoDateUtils.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoDateUtils.h"

@interface PWADemoDateUtils ()

/**
 *  Calculates and returns the date that the given weekday occurs in the upcoming week
 *  @param dayNumber    The number corresponding to the desired weekday
 *  @return             The desired date in the form of a string detailing month and day
 */
+ (NSString *)getDateInNextWeekForDay:(NSInteger)dayNumber;

@end

@implementation PWADemoDateUtils

const int kDaysInWeek = 7;
const int kMonday = 2;
const int kFriday = 6;

+ (NSString *)getNextBusinessWeekDates
{
    NSString *nextMonday = [PWADemoDateUtils getDateInNextWeekForDay:kMonday];
    NSString *nextFriday = [PWADemoDateUtils getDateInNextWeekForDay:kFriday];
    return [NSString stringWithFormat:@"%@ - %@", nextMonday, nextFriday];
}

+ (NSString *)getDateInNextWeekForDay:(NSInteger)dayNumber
{
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    NSDateComponents *todayWeekdayComponents = [calendar components:NSCalendarUnitWeekday fromDate:today];
    NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
    [componentsToAdd setDay:(kDaysInWeek - ([todayWeekdayComponents weekday] - dayNumber))];
    
    NSDate *desiredDate = [calendar dateByAddingComponents:componentsToAdd toDate:today options:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd"];
    return [dateFormatter stringFromDate:desiredDate];
}

@end
