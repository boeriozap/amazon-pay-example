//
//  PWADemoProductDetailsViewController.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWADemoProduct.h"

@interface PWADemoProductDetailsViewController : UIViewController

/**
 *  The selected product that is displayed during the scene
 */
@property PWADemoProduct *selectedProduct;

@end
