//
//  PWADemoXMLParser.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  An XML parser used to parse getOrderReferenceDetails responses. This is an imperfect parser.
 *  It will not display all of the information in the order reference object.
 */
@interface PWADemoXMLParser : NSObject <NSXMLParserDelegate>

- (instancetype)initWithCompleteHandler: (void (^)(NSDictionary *, NSError *))onParseCompletion;

@end