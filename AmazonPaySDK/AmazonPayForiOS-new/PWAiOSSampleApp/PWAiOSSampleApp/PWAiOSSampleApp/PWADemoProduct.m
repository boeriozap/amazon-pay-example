//
//  PWADemoProduct.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoProduct.h"

@implementation PWADemoProduct

static NSArray<PWADemoProduct *> *presetProducts;

- (instancetype)init
{
    return [self initWithProduct:[presetProducts lastObject]];
}

- (instancetype)initWithProduct:(PWADemoProduct *)product
{
    return [self initWithProductName:product.productName
                               image:product.productImageName
                               color:product.productColor
                               price:product.unitPrice];
}

- (instancetype)initWithProductName:(NSString *)name image:(NSString *)image color:(NSString *)color price:(NSDecimalNumber *)price
{
    self = [super init];
    _productName = [[NSString alloc] initWithString:name];
    _productImageName = [[NSString alloc] initWithString:image];
    _productColor = [[NSString alloc] initWithString:color];
    _unitPrice = [[NSDecimalNumber alloc] initWithDecimal:price.decimalValue];
    return self;
}

- (instancetype)initWithPresetProductNumber:(NSUInteger)productNumber
{
    if (productNumber > presetProducts.count) {
        return [presetProducts lastObject];
    } else {
        return [presetProducts objectAtIndex:productNumber];
    }
}

- (BOOL)isEqual:(id)object
{
    // Returns YES if object is of type (PWADemoProduct *), and has identical product name as self
    return [object class] == [PWADemoProduct class] && [self.productName isEqualToString:((PWADemoProduct *)object).productName];
}

+ (NSInteger)presetProductCount
{
    return presetProducts.count;
}

+ (void)initialize
{
    presetProducts = @[ [[PWADemoProduct alloc] initWithProductName:@"Painted Flower iPhone Case"
                                                              image:@"PaintedFloweriPhoneCaseByLUOLNH.jpg"
                                                              color:@"Green"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"1.70"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Moody Banana iPhone Case"
                                                              image:@"MoodyBananaiPhoneCase@2x.png"
                                                              color:@"Green"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"27.00"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Hubble Scope Space iPhone Case"
                                                              image:@"TexturedHubbleScopeColorfulConstellationStarsOuterSpaceCaseByInspiredCases.png"
                                                              color:@"Multi"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"13.99"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Hippy Hand iPhone Case"
                                                              image:@"HippyHandiPhoneCase@2x.png"
                                                              color:@"Multi"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"32.00"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Magic Triangle iPhone Case"
                                                              image:@"MagicTriangleiPhoneCase@2x.png"
                                                              color:@"Multi"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"32.00"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Pineapple Lover iPhone Case"
                                                              image:@"PineappleLoveriPhoneCaseByLUOLNH.jpg"
                                                              color:@"Pink"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"1.99"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Wood Texture iPhone Case"
                                                              image:@"ColoredWoodTextureiPhoneCaseByDesignerCases.jpg"
                                                              color:@"Multi"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"4.68"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Boston Terrier iPhone Case"
                                                              image:@"BostonTerrieriPhoneCase@2x.png"
                                                              color:@"Yellow"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"28.00"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Hand Drawn Peacock iPhone Case"
                                                              image:@"HandDrawnPeacockiPhoneCaseByChiChiC.png"
                                                              color:@"Multi"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"12.99"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Dogs And Cats iPhone Case"
                                                              image:@"DogsAndCatsiPhoneCaseByChiChiC.jpg"
                                                              color:@"Brown"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"9.99"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Balloon House iPhone Case"
                                                              image:@"ColorfulHouseCaseByOnebyOneShop.jpg"
                                                              color:@"Multi"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"3.47"]],
                        [[PWADemoProduct alloc] initWithProductName:@"Pineapple Fashion iPhone Case"
                                                              image:@"PineappleFashioniPhoneCaseBySandistore.jpg"
                                                              color:@"White"
                                                              price:[NSDecimalNumber decimalNumberWithString:@"1.34"]]
                        ];
}

@end
