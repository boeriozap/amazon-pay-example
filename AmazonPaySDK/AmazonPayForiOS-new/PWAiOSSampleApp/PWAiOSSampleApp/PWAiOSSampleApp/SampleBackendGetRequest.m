//
//  SampleBackendGetRequest.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendGetRequest.h"
#import "SampleBackendURLUtils.h"
#import "SampleBackendErrorFactory.h"
#import "PWADemoConstants.h"

@implementation SampleBackendGetRequest

- (NSURLRequest *)generateRequest
{
    NSURL *url = [SampleBackendURLUtils buildURLForHost:self.host path:self.path URLParams:[self requestQueryItems]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setHTTPMethod:kHttpMethodGet];
    
    return urlRequest;
}

@end
