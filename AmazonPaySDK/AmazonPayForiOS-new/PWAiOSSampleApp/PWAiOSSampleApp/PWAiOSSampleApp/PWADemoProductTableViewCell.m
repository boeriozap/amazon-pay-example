//
//  PWADemoProductTableViewCell.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoProductTableViewCell.h"
#import "PWADemoCart.h"
#import "PWADemoCurrencyFormatter.h"

@interface PWADemoProductTableViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productQuantityLabel;

@property PWADemoCartItem *cartItem;

/**
 *  Sets the product image view to display the product's image
 */
- (void)formatProductImageView;

/**
 *  Sets the product name label to display the product's name
 */
- (void)formatProductNameLabel;

/**
 *  Sets and formats the product price label to display the product's unit price
 */
- (void)formatProductPriceLabel;

/**
 *  Displays the quantity of the cell's cart item
 */
- (void)updateView;

/**
 *  Increases the quantity of the cell's cart item by one
 */
- (IBAction)incrementQuantity:(id)sender;

/**
 *  Decreases the quantity of the cell's cart item by one. If the quantity becomes zero, the cell is removed
 */
- (IBAction)decrementQuantity:(id)sender;

@end

@implementation PWADemoProductTableViewCell

- (void)setDisplayWithCartItem:(PWADemoCartItem *)cartItem
{
    self.cartItem = cartItem;
    
    [self formatProductImageView];
    [self formatProductNameLabel];
    [self formatProductPriceLabel];
    
    [self updateView];
}

- (void)formatProductImageView
{
    self.productImageView.image = [UIImage imageNamed:self.cartItem.product.productImageName];
}

- (void)formatProductNameLabel
{
    self.productNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.productNameLabel.numberOfLines = 0;
    self.productNameLabel.text = self.cartItem.product.productName;
}

- (void)formatProductPriceLabel
{
    self.productPriceLabel.text = [PWADemoCurrencyFormatter stringFromNumber:self.cartItem.product.unitPrice];
}

- (void)updateView
{
    self.productQuantityLabel.text = [NSString stringWithFormat:@"%ld", (long)(self.cartItem.quantity)];
}

- (IBAction)incrementQuantity:(id)sender
{
    [[PWADemoCart sharedCart] addItemToCart:self.cartItem.product
                               withQuantity:1];
    [self updateView];
}

- (IBAction)decrementQuantity:(id)sender
{
    [[PWADemoCart sharedCart] subtractItemFromCart:self.cartItem.product];
    [self updateView];
}
     
@end
