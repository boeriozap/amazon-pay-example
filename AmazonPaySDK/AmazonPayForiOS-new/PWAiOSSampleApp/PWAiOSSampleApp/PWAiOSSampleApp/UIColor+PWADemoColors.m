//
//  UIColor+PWADemoColors.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "UIColor+PWADemoColors.h"

@implementation UIColor (PWADemoColors)

+ (UIColor *)lightOrangeColor
{
    return [UIColor colorWithRed:0.94 green:0.62 blue:0.51 alpha:1.0];
}

+ (UIColor *)extraLightGrayColor
{
    return [UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1.0];
}

+ (UIColor *)torquoiseColor
{
    return [UIColor colorWithRed:0.17 green:0.73 blue:0.71 alpha:1.0];
}

+ (UIColor *)limeGreenColor
{
    return [UIColor colorWithRed:0.45 green:0.87 blue:0.34 alpha:1.0];
}

@end
