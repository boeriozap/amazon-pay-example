//
//  PWADemoPickerData.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWADemoPickerData : NSObject

+ (NSArray<NSString *> *)localeIdentifiers;

@end