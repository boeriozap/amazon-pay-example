//
//  PWADemoConstants.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWADemoConstants : NSObject

extern NSString *const kGetOrderReferenceDetailsEndpointPath;
extern NSString *const kGetAuthorizationDetailsEndpointPath;
extern NSString *const kGetCaptureDetailsEndpointPath;
extern NSString *const kGenerateSignatureEndpointPath;
extern NSString *const kGetDetailsAmazonOrderReferenceIDParameterName;
extern NSString *const kGetDetailsAmazonAuthorizationIDParameterName;
extern NSString *const kGetDetailsAmazonCaptureIDParameterName;
extern NSString *const kGetDetailsAmazonAccessTokenParameterName;

extern NSString *const kHttpMethodPost;
extern NSString *const kHttpMethodGet;

@end
