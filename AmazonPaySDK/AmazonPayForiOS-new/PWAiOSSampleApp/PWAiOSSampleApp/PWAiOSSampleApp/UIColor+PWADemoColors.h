//
//  UIColor+PWADemoColors.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PWADemoColors)

/**
 *  Returns a light orange color used for prices
 */
+ (UIColor *) lightOrangeColor;

/**
 *  Returns an extra light gray color used for the product list's background
 */
+ (UIColor *) extraLightGrayColor;

/**
 *  Returns a torquoise color used in the logo, change quantity buttons, add to cart button, checkout button, place order button
 */
+ (UIColor *) torquoiseColor;

/**
 *  Returns a lime green color used to signify that an item was added to the cart
 */
+ (UIColor *) limeGreenColor;

@end
