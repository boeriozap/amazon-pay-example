//
//  SampleBackendGenerateSignatureRequest.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendGenerateSignatureRequest.h"
#import "SampleBackendURLUtils.h"
#import "PWADemoConstants.h"

@implementation SampleBackendGenerateSignatureRequest

- (instancetype)init {
    self = [super init];
    if (self) {
        self.path = kGenerateSignatureEndpointPath;
    }
    return self;
}

- (instancetype)initWithTextToSign: (NSString *)textToSign {
    self = [self init];
    if (self) {
        self.textToSign = textToSign;
    }
    
    return self;
}

- (NSData *) requestBody {
    return [self.textToSign dataUsingEncoding: NSUTF8StringEncoding];
}

@end
