//
//  PWADemoConstants.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoConstants.h"

@implementation PWADemoConstants

NSString *const kGetOrderReferenceDetailsEndpointPath = @"/GetOrderReferenceDetails";
NSString *const kGetAuthorizationDetailsEndpointPath = @"/GetOrderReferenceDetails";
NSString *const kGetCaptureDetailsEndpointPath = @"/GetOrderReferenceDetails";
NSString *const kGenerateSignatureEndpointPath = @"/SignRequest";

NSString *const kGetDetailsAmazonOrderReferenceIDParameterName = @"AmazonOrderReferenceId";
NSString *const kGetDetailsAmazonAuthorizationIDParameterName = @"AmazonAuthorizationid";
NSString *const kGetDetailsAmazonCaptureIDParameterName = @"AmazonCaptureId";
NSString *const kGetDetailsAmazonAccessTokenParameterName = @"AccessToken";

NSString *const kHttpMethodPost = @"POST";
NSString *const kHttpMethodGet = @"GET";

@end
