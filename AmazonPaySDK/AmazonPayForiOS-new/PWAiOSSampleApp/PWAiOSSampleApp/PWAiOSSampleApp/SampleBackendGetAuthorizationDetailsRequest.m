//
//  SampleBackendGetAuthorizationDetailsRequest.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "SampleBackendGetAuthorizationDetailsRequest.h"
#import "PWADemoConstants.h"

@implementation SampleBackendGetAuthorizationDetailsRequest

- (instancetype)init {
    self = [super init];
    if (self) {
        self.path = kGetAuthorizationDetailsEndpointPath;
    }
    return self;
}

- (instancetype)initWithAmazonAuthorizationID:(NSString *)amazonAuthorizationID {
    self = [self init];
    if (self) {
        self.amazonAuthorizationID = amazonAuthorizationID;
    }
    
    return self;
}

- (NSDictionary *)requestQueryItems {
    NSMutableDictionary *queryStrings = [NSMutableDictionary dictionary];
    queryStrings[kGetDetailsAmazonAuthorizationIDParameterName] = self.amazonAuthorizationID;
    
    return queryStrings;
}

@end
