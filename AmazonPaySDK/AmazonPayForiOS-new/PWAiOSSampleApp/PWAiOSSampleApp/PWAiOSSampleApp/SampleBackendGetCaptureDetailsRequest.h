//
//  SampleBackendGetCaptureDetailsRequest.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SampleBackendGetRequest.h"

@interface SampleBackendGetCaptureDetailsRequest : SampleBackendGetRequest

@property (nonatomic, copy) NSString *amazonCaptureID;

- (instancetype)initWithAmazonCaptureID:(NSString *)amazonCaptureID;

@end
