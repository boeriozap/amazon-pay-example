//
//  PWADemoDateUtils.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWADemoDateUtils : NSObject

/**
 *  Calculates and returns the first and last days of the next business week
 *  @return A string detailing the first and last dats of the next business week
 */
+ (NSString *)getNextBusinessWeekDates;

@end