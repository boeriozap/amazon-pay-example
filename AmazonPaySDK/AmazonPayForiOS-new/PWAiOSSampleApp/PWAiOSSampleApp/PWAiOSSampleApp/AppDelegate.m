//
//  AppDelegate.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "AppDelegate.h"
#import "PWADemoSettingsViewController.h"
#import "PWADemoRegionUtils.h"
#import "PWADemoCurrencyFormatter.h"
#import "PWADemoDefaultSettings.h"
#import <LoginWithAmazon/LoginWithAmazon.h>
@import PayWithAmazon;

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url sourceApplication:(nullable NSString *)sourceApplication annotation:(nonnull id)annotation
{
    return [AMZNAuthorizationManager handleOpenURL:url sourceApplication:sourceApplication];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Login and AmazonPay defaults initialization. Setting the default region and ledger currency for Pay with Amazon is required.
    [AMZNAuthorizationManager sharedManager].region = [PWADemoRegionUtils lwaRegionFromString:[PWADemoDefaultSettings sharedSettings].defaultLWARegion];
    [AmazonPay setDefaultRegion:[PWADemoRegionUtils pwaRegionFromString:[PWADemoDefaultSettings sharedSettings].defaultPWARegion]];
    [AmazonPay setDefaultLedgerCurrency:[PWADemoDefaultSettings sharedSettings].defaultLedgerCurrency];
    [AmazonPay setDefaultLocale:[NSLocale localeWithLocaleIdentifier:[PWADemoDefaultSettings sharedSettings].defaultLocaleIdentifier]];
    
#if US_MERCHANT
    NSLog(@"US Merchant settings have been set");
#elif UK_MERCHANT
    NSLog(@"UK Merchant settings have been set");
#elif DE_MERCHANT
    NSLog(@"DE Merchant settings have been set");
#elif JP_MERCHANT
    NSLog(@"JP Merchant settings have been set");
#endif
    NSLog(@"Default LWA region set to %@", [PWADemoDefaultSettings sharedSettings].defaultLWARegion);
    NSLog(@"Default PWA region set to %@", [PWADemoDefaultSettings sharedSettings].defaultPWARegion);
    NSLog(@"Default ledger currency set to %@", [PWADemoDefaultSettings sharedSettings].defaultLedgerCurrency);
    NSLog(@"Default locale identifier set to %@", [PWADemoDefaultSettings sharedSettings].defaultLocaleIdentifier);
    
    // Currency formatter initialization
    [PWADemoCurrencyFormatter setLocaleIdentifierWithCurrency:[PWADemoDefaultSettings sharedSettings].defaultLedgerCurrency];
    
    // Comment out the following line when ready for production or explicitly set it to false.
    [AMZNAuthorizationManager sharedManager].sandboxMode = YES;
    NSLog(@"Sandbox");
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
