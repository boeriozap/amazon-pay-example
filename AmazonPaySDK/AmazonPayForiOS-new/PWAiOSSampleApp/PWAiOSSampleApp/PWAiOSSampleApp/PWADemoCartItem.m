//
//  PWADemoCartItem.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoCartItem.h"

@implementation PWADemoCartItem

- (instancetype)initWithProduct:(PWADemoProduct *)product quantity:(NSUInteger)quantity
{
    _product = [[PWADemoProduct alloc] initWithProduct:product];
    _quantity = quantity;
    return self;
}

- (BOOL)isEqual:(id)object
{
    return [PWADemoCartItem class] && [self.product.productName isEqualToString:((PWADemoCartItem *)object).product.productName];
}

- (void)addQuantity:(NSUInteger)quantityToAdd
{
    [self updateQuantity:self.quantity + quantityToAdd];
}

- (void)subtractQuantityByOne
{
    [self updateQuantity:self.quantity - 1];
}

- (void)updateQuantity:(NSUInteger)newQuantity
{
    _quantity = newQuantity;
}


@end
