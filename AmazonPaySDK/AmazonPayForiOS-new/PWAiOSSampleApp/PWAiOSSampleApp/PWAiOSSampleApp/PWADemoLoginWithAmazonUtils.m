//
//  PWADemoLoginWithAmazonUtils.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoLoginWithAmazonUtils.h"
#import "PWADemoAmazonAccount.h"
#import "PWADemoAlertMessageUtils.h"
@import PayWithAmazon;

@implementation PWADemoLoginWithAmazonUtils

+ (void)loginWithAmazonWithCompletionHandler:(PWADemoLoginHandler)completionHandler
{
    /**
     *  Build an authorize request with your desired scopes.
     *  [AMZNProfileScope profile] returns user name, email, and Amazon account ID
     *  [PWAPaymentScope initiate] (required) provides permission to process a payment
     *  [PWAPaymentScope shippingAddress] returns buyer's shipping address
     *  [PWAPaymentScope instrument] provides permission to access short description of buyer's payment instrument
     */
    AMZNAuthorizeRequest *request = [[AMZNAuthorizeRequest alloc] init];
    request.scopes = [NSArray arrayWithObjects:[AMZNProfileScope profile],
                                               [APayPaymentScope initiate],
                                               [APayPaymentScope shippingAddress],
                                               [APayPaymentScope instrument],
                                               nil];
    
    // Make an Authorize call to the Login with Amazon SDK
    NSLog(@"Making Authorize LWA API call");
    [[AMZNAuthorizationManager sharedManager] authorize:request
                                            withHandler:completionHandler];
}

+ (void)logoutOfAmazonWithCompletionHandler:(PWADemoLogoutHandler)completionHandler
{
    // Make a sign out call to the Login with Amazon SDK
    NSLog(@"Making Sign Out LWA API call");
    [[AMZNAuthorizationManager sharedManager] signOut:completionHandler];
}

+ (PWADemoLoginHandler)buildLoginHandlerWithHandler:(void (^)(AMZNAuthorizeResult *, NSError *))supplementaryHandler
{
    return ^(AMZNAuthorizeResult *result, BOOL userDidCancel, NSError *error) {
        if (error) {
            NSLog(@"Login with Amazon failed");
            NSLog(@"LWA error: %@", error.description);
            if (userDidCancel) {
                [PWADemoAlertMessageUtils setAlertMessage:@"Canceled login"];
            } else {
                [PWADemoAlertMessageUtils setAlertMessage:@"Failed to login"];
            }
        } else {
            NSLog(@"Login with Amazon succeeded");
            NSLog(@"Amazon user name: %@", result.user.name);
            NSLog(@"Amazon user email: %@", result.user.email);
                
            // Integrate with your existing account management system (if applicable)
            
        }
        [[PWADemoAmazonAccount sharedInstance] setLoggedIn:(error == nil)];
        
        supplementaryHandler(result, error);
    };
}

+ (PWADemoLogoutHandler)buildLogoutHandlerWithHandler:(void (^)(NSError *))supplementaryHandler
{
    return ^(NSError *error) {
        if (error) {
            NSLog(@"Logout of Amazon failed");
            NSLog(@"LWA error: %@", error.description);
            [PWADemoAlertMessageUtils setAlertMessage:@"Failed to logout"];
        } else {
            NSLog(@"Logout of Amazon succeeded");
        
            // Integrate with your existing account management system (if applicable)
            
        }
        [[PWADemoAmazonAccount sharedInstance] setLoggedIn:!(error == nil)];
        
        supplementaryHandler(error);
    };
}

@end
