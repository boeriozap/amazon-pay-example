//
//  PWADemoOrderReference.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoOrderReference.h"
#import "PWADemoAlertMessageUtils.h"

@interface PWADemoOrderReference ()

/**
 *  Sets shipping address street using address line 1 and line 2
 */
- (void)setShippingAddressStreetWithLine1:(NSString *)addressLine1 line2:(NSString *)addressLine2;

/**
 *  Sets shipping address general using city, region, postal code, and country. The general address can be
 *  formatted in a few different ways depending on which parameters (if any) are nil.
 */
- (void)setShippingAddressGeneralWithCity:(NSString *)city region:(NSString *)region postalCode:(NSString *)postalCode country:(NSString *)country;

/**
 *  Sets payment instrument by formatting the payment descriptor name and the account number tail
 */
- (void)setPaymentInstrumentWithDescriptorName:(NSString *)descriptorName accountNumberTail:(NSString *)accountNumberTail;

@end

@implementation PWADemoOrderReference

NSString *const kGetOrderReferenceDetailsResult = @"getOrderReferenceDetailsResult";
NSString *const kOrderReferenceDetails = @"orderReferenceDetails";
NSString *const kOrderReferenceDestination = @"destination";
NSString *const kOrderReferencePhysicalDestination = @"physicalDestination";


NSString *const kDestinationAddressLine1 = @"addressLine1";
NSString *const kDestinationAddressLine2 = @"addressLine2";
NSString *const kDestinationCity = @"city";
NSString *const kDestinationCountry = @"countryCode";
NSString *const kDestinationStateOrRegion = @"stateOrRegion";
NSString *const kDestinationPostalCode = @"postalCode";
NSString *const kPaymentDescriptorName = @"PaymentDescriptor.Name";
NSString *const kAccountNumberTail = @"PaymentDescriptor.AccountNumberTail";
NSString *const kBuyerEmail = @"email";
NSString *const kBuyerName = @"name";

const NSInteger kArbitraryNumber = 14159;

#pragma mark - Public

- (instancetype)initWithAmazonOrderReferenceId:(NSString *)amazonOrderReferenceId;
{
    // Set arbitrary seller order id
    static NSInteger orderNumber = kArbitraryNumber;
    _sellerOrderId = [NSString stringWithFormat:@"SWB3P%ld", orderNumber];
    orderNumber++;
    
    _amazonOrderReferenceId = amazonOrderReferenceId;
    
    return self;
}

- (void)setOrderDetailsWithDictionary:(NSDictionary *)detailsDictionary
{
    NSDictionary *orderDetails = detailsDictionary[kGetOrderReferenceDetailsResult][kOrderReferenceDetails];
    if (orderDetails[kOrderReferenceDestination][kOrderReferencePhysicalDestination]) {
        NSDictionary *physicalDestination = orderDetails[kOrderReferenceDestination][kOrderReferencePhysicalDestination];
        if (physicalDestination[kDestinationAddressLine1] || physicalDestination[kDestinationAddressLine2]) {
            [self setShippingAddressStreetWithLine1:physicalDestination[kDestinationAddressLine1]
                                              line2:physicalDestination[kDestinationAddressLine2]];
            [self setShippingAddressGeneralWithCity:physicalDestination[kDestinationCity]
                                             region:physicalDestination[kDestinationStateOrRegion]
                                         postalCode:physicalDestination[kDestinationPostalCode]
                                            country:physicalDestination[kDestinationCountry]];
        } else {
            NSLog(@"No address associated");
            [PWADemoAlertMessageUtils setAlertMessage:@"No address associated\n\nAdd shipping address to Amazon account"];
        }
    } else {
        NSLog(@"No address associated");
        [PWADemoAlertMessageUtils setAlertMessage:@"No address associated\n\nAdd shipping address to Amazon account"];
    }
   
    // TODO. Add payment instruments parser after Backend SDK is fixed.
    /*if (detailsDictionary[kPaymentDescriptorName]) {
        [self setPaymentInstrumentWithDescriptorName:detailsDictionary[kPaymentDescriptorName]
                                   accountNumberTail:detailsDictionary[kAccountNumberTail]];
    } else {
        NSLog(@"No payment instrument");
        [PWADemoAlertMessageUtils setAlertMessage:@"No payment instrument\n\nAdd payment method to Amazon account"];
    }*/
    
    if (orderDetails[kOrderReferenceDetails][kBuyerEmail]) {
        _buyerEmail = orderDetails[kOrderReferenceDetails][kBuyerEmail];
    } else {
        _buyerEmail = @"No customer email";
    }
    
    if (orderDetails[kOrderReferenceDetails][kBuyerName]) {
        _buyerName = orderDetails[kOrderReferenceDetails][kBuyerName];
    } else {
        _buyerName = @"No customer name";
    }
}

#pragma mark - Private

- (void)setShippingAddressStreetWithLine1:(NSString *)addressLine1 line2:(NSString *)addressLine2
{
    if (addressLine1 && addressLine2) {
        _shippingAddressStreet = [NSString stringWithFormat:@"%@, %@", addressLine1, addressLine2];
    } else if (addressLine1) {
        _shippingAddressStreet = addressLine1;
    } else if (addressLine2) {
        _shippingAddressStreet = addressLine2;
    } else {
        _shippingAddressStreet = @"No street address";
    }
}

- (void)setShippingAddressGeneralWithCity:(NSString *)city region:(NSString *)region postalCode:(NSString *)postalCode country:(NSString *)country
{
    NSMutableString *shippingAddressGeneral = nil;
    
    if (city && region) {
        shippingAddressGeneral = [NSMutableString stringWithFormat:@"%@, %@", city, region];
    } else if (city) {
        shippingAddressGeneral = [NSMutableString stringWithString:city];
    } else if (region) {
        shippingAddressGeneral = [NSMutableString stringWithString:region];
    }
    
    if (shippingAddressGeneral && postalCode) {
        [shippingAddressGeneral appendString:[NSString stringWithFormat:@" %@", postalCode]];
    } else if (postalCode) {
        shippingAddressGeneral = [NSMutableString stringWithString:postalCode];
    }
    
    if (shippingAddressGeneral && country) {
        [shippingAddressGeneral appendString:[NSString stringWithFormat:@", %@", country]];
    } else if (country) {
        shippingAddressGeneral = [NSMutableString stringWithString:country];
    }
    
    _shippingAddressGeneral = shippingAddressGeneral;
}

- (void)setPaymentInstrumentWithDescriptorName:(NSString *)descriptorName accountNumberTail:(NSString *)accountNumberTail
{
    _paymentInstrument = [NSString stringWithFormat:@"%@ **%@", descriptorName, accountNumberTail];
}

@end
