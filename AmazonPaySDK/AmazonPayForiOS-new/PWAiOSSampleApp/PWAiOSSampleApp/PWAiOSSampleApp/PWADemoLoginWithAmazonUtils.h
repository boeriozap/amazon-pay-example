//
//  PWADemoLoginWithAmazonUtils.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <LoginWithAmazon/LoginWithAmazon.h>
#import "UIActivityIndicatorView+PWADemoActivityIndicator.h"

typedef void (^PWADemoLoginHandler)(AMZNAuthorizeResult *, BOOL, NSError *);
typedef void (^PWADemoLogoutHandler)(NSError *error);

/**
 *  The PWADemoLoginWithAmazonUtils class contains methods that prepare for and make Login with Amazon API calls.
 */
@interface PWADemoLoginWithAmazonUtils : NSObject

/**
 *  Attempt to Login with Amazon. In this method, we build an AMZNAuthorizeRequest using the scopes that 
 *  we want. Note that the payments initiate scope is the only scope that is required for processing a
 *  payment. We then use the AMZNAuthorizationManager shared manager to call the authorize:withHandler
 *  LWA API call using the AMZNAuthorizeRequest and a completion handler. This authorize call will take
 *  the user to an Amazon login page, where they will input their email and Amazon password. If the user
 *  is already signed in on their Amazon app, then this login process will be skipped, as they are already
 *  logged in due to Single Sign On. Upon successful login, the completion handler will be called with an
 *  AMZNAuthorizeResult containing an access token and the buyer information requested by the scopes. If
 *  login fails, the completion handler is called with the error set.
 *
 *  @param completionHandler Completion handler to run on Login with Amazon success or failure
 */
+ (void)loginWithAmazonWithCompletionHandler:(PWADemoLoginHandler)completionHandler;

/**
 *  Attempt to logout of Amazon. In this method, we use the AMZNAuthorizationManager shared manager to make
 *  the signOut LWA API call with a completion handler. Upon success, the completion handler will be called
 *  with no error set. Upon failure, the completion handler is called with error set.
 *
 *  @param completionHandler Completion handler to run on logout of Amazon success or failure
 */
+ (void)logoutOfAmazonWithCompletionHandler:(PWADemoLogoutHandler)completionHandler;

/**
 *  Build a completion handler for use in Login with Amazon attempt. The completion handler will check for
 *  and log errors. Upon success, buyer name and email will be logged. If you have your own account management
 *  system, then you should integrate on succesful login. After the error check, the supplementary handler is
 *  called. Use this for any customization needed in the completion handler.
 *
 *  @param supplementaryHandler Supplementary handler to be inserted at the end of login completion handler
 *  @return                     Completion handler for use in Login with Amazon attempt
 */
+ (PWADemoLoginHandler)buildLoginHandlerWithHandler:(void (^)(AMZNAuthorizeResult *, NSError *))supplementaryHandler;

/**
 *  Build a completion handler for use in logout of Amazon attempt. The completion handler will check for
 *  and log errors. If you have your own account management system, then you should integrate on successful
 *  logout. After the error check, the supplementary handler is called. Use this for any customiation
 *  needed in the completion handler.
 *
 *  @param supplementaryHandler Supplementary handler to be inserted at the end of logout completion handler
 *  @return                     Completion handler for use in logout of Amazon attempt
 */
+ (PWADemoLogoutHandler)buildLogoutHandlerWithHandler:(void (^)(NSError *))supplementaryHandler;

@end
