//
//  PWADemoAmazon.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWADemoAmazonAccount : NSObject

+ (instancetype)sharedInstance;

/**
 * Indicates if the user has logged in using their Amazon account
 */
@property BOOL loggedIn;

@end
