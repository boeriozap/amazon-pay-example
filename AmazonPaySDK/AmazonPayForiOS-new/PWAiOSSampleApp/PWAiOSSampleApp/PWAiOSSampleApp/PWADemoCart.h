//
//  PWADemoCart.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PWADemoProduct.h"
#import "PWADemoCartItem.h"

/**
 *  The PWADemoCart class contains a list of items that have been added to the cart as well
 *  as methods to alter the contents of the cart, and methods to calculate price breakdowns.
 */
@interface PWADemoCart : NSObject

/**
 *  Returns shared instance of cart
 */
+ (PWADemoCart *)sharedCart;

/**
 *  Initializes an empty cart with no items and zero cost
 */
- (instancetype)init;

/**
 *  Returns the number of items in the cart
 */
- (NSUInteger)itemCount;

/**
 *  Checks if a product is an item in the cart
 *  @param product The product that is checked for
 */
- (BOOL)itemIsInCart:(PWADemoProduct *)product;

/**
 *  Gets the cart item in cart at the index provided
 *  @param cartIndex    The index of the cart item
 *  @return             The desired cart item
 */
- (PWADemoCartItem *)getCartItemAtIndex:(NSUInteger)cartIndex;

/**
 *  Adds an item to the cart. If the item is already in the cart, the quantity of that item is incremented instead
 *  @param productToAdd     The product of the item that will be added to the cart
 *  @param quantityToAdd    The quantity of the given product that will be added to the cart
 */
- (void)addItemToCart:(PWADemoProduct *)productToAdd withQuantity:(NSUInteger)quantityToAdd;

/**
 *  Subtracts one unit of the given item from the cart. If quantity goes below one, the item is removed from cart
 *  @param productToSubtract The product of the item that will have some of its units removed from the cart
 */
- (void)subtractItemFromCart:(PWADemoProduct *)productToSubtract;

/**
 *  Removes the item at the given index
 *  @param cartIndex The index of the item to be removed
 */
- (void)removeItemAtIndex:(NSUInteger)cartIndex;

/**
 *  Empties all items from the cart
 */
- (void)emptyCart;

/**
 *  @return The total cost of the items in the cart, not including shipping and tax
 */
- (NSDecimalNumber *)getTotalCost;

/**
 *  @return The calculated shipping cost of the cart
 */
- (NSDecimalNumber *)getShippingCost;

/**
 *  @return The calculated tax amount based off the cost of the items in the cart plus shipping cost
 */
- (NSDecimalNumber *)getTaxAmount;

/**
 *  @return The total cost of items in the cart plus the calculated shipping and tax costs
 */
- (NSDecimalNumber *)getTotalCostWithShippingAndTax;

@end
