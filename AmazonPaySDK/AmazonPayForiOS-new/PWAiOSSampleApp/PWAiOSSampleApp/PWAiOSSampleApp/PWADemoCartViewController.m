//
//  PWADemoCartViewController.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoCartViewController.h"
#import "PWADemoAmazonAccount.h"
#import "PWADemoCart.h"
#import "UIColor+PWADemoColors.h"
#import "UIBarButtonItem+PWADemoCartIcon.h"
#import "PWADemoProductTableViewCell.h"
#import "PWADemoCurrencyFormatter.h"
#import "UIViewController+PWADemoAlertMessage.h"
#import "PWADemoLoginWithAmazonUtils.h"
#import "PWADemoOrderReference.h"
#import "PWADemoPayWithAmazonUtils.h"
#import "PWADemoAlertMessageUtils.h"
#import "PWADemoReviewOrderViewController.h"
#import <LoginWithAmazon/LoginWithAmazon.h>

/**
 *  The PWADemoCartController controls the Cart scene.
 *  The user arrives at this scene via the Product List or Product Detail scenes.
 *  In this scene, the user can scroll through the items in their cart, and view the price breakdown of their would-be purchase
 *  The user is able to begin the payment process by pressing the checkout button. If the user is logged in with Amazon they go
 *  straight to the Review Order scene.  Otherwise, a pop-up appears displaying the payment options.
 *  The user is able to navigate back to the previous scene by pressing the back button in the top left corner.
 */
@interface PWADemoCartViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *merchandisePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCostOfPurchaseLabel;
@property (weak, nonatomic) IBOutlet UIView *itemsInCartView;
@property (weak, nonatomic) IBOutlet UITableView *itemsInCartTableView;
@property (weak, nonatomic) IBOutlet UIButton *dimViewButton;
@property (weak, nonatomic) IBOutlet UIView *paymentPopUpView;
@property (weak, nonatomic) IBOutlet UIButton *payWithCreditCardButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cartBarButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *checkoutButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property NSString *alertMessage;

@property PWADemoOrderReference *order;

/**
 *  Displays an alert message if one is necessary
 */
- (void)viewDidAppear:(BOOL)animated;

/**
 *  Displays the cart information and intializes cart icon
 */
- (void)viewWillLayoutSubviews;

/**
 *  Prepares the payments pop up
 */
- (void)viewDidLayoutSubviews;

/**
 *  Displays the price breakdown calculated from the items in the cart
 */
- (void)displayPriceBreakdown;

/**
 *  Displays the current price breakdown, the current contents of the cart, and the current cart quantity
 */
- (void)updateView;

/**
 *  Returns the number of rows in table view section which is equal to the number of items in the cart
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

/**
 *  Returns the number of sections in the table view which is one
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;

/**
 *  Returns the cell at the specified index path after setting the cell to display its corresponding cart item
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

/**
 *  Returns the height of the cell specified by the index path
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

/**
 *  Defines and returns the remove edit action for the cell specified by index path
 */
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath;

/**
 *  The user will Login with Amazon. On success, user info is used in integration with merchant account system, and the Pay with Amazon flow begins.
 *  On failure, an alert message is displayed.
 */
- (IBAction)loginAndPayWithAmazon:(id)sender;

/**
 *  If cart not empty, then if logged in to Amazon account, segues to review order page, else dims background and brings payment options into view
 */
- (IBAction)checkout:(id)sender;

/**
 *  Lightens the background and moves the payment options pop-up out of view
 */
- (IBAction)hidePaymentOptionPopUp:(id)sender;

/**
 *  Dims the background and moves the payment options pop-up into view
 */
- (void)showPaymentsOptionPopUp;

@end

@implementation PWADemoCartViewController

const int kProductTableViewSectionCount = 1;
const double kProductTableCellHeightMultiplier = 0.4;
const double kPaymentsPopUpAnimationDuration = 0.3;
const double kDimViewVisibleAlpha = 0.5;
const double kDimViewInvisibleAlpha = 0;
const int kHidePaymentsPopUpHorizontalMultiplier = 2;

NSString *const kGoToReviewOrderPageSegue = @"goToReviewOrder";
NSString *const kProductTableCell = @"productTableCell";

#pragma mark - Display

- (void)viewDidAppear:(BOOL)animated
{
    if ([PWADemoAlertMessageUtils alertMessageIsSet]) {
        [self raiseAlertMessage:[PWADemoAlertMessageUtils getAndClearAlertMessage]];
    }
}

- (void)viewWillLayoutSubviews
{
    [self updateView];
}

- (void)viewDidLayoutSubviews
{
    self.itemsInCartView.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.checkoutButton.userInteractionEnabled = [[PWADemoCart sharedCart] itemCount] > 0;
    
    // Initialize payment pop up view for future use
    self.paymentPopUpView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.paymentPopUpView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.payWithCreditCardButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.view bringSubviewToFront:self.dimViewButton];
    [self.view bringSubviewToFront:self.paymentPopUpView];
    
    // Move the pop up off screen initially
    [self.paymentPopUpView setCenter:CGPointMake(self.view.frame.size.width * kHidePaymentsPopUpHorizontalMultiplier, self.paymentPopUpView.center.y)];
    self.dimViewButton.hidden = YES;
    
    [self.view bringSubviewToFront:self.activityIndicator];
    [self.activityIndicator setColor:[UIColor grayColor]];
}

- (void)displayPriceBreakdown
{
    self.merchandisePriceLabel.text = [PWADemoCurrencyFormatter stringFromNumber:[[PWADemoCart sharedCart] getTotalCost]];
    self.shippingPriceLabel.text = [PWADemoCurrencyFormatter stringFromNumber:[[PWADemoCart sharedCart] getShippingCost]];
    self.taxPriceLabel.text = [PWADemoCurrencyFormatter stringFromNumber:[[PWADemoCart sharedCart] getTaxAmount]];
    self.totalCostOfPurchaseLabel.text = [PWADemoCurrencyFormatter stringFromNumber:[[PWADemoCart sharedCart] getTotalCostWithShippingAndTax]];
}

- (void)updateView
{
    [self displayPriceBreakdown];
    [self.itemsInCartTableView reloadData];
    [self.cartBarButtonItem updateCartQuantity];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[PWADemoCart sharedCart] itemCount];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kProductTableViewSectionCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PWADemoProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kProductTableCell forIndexPath:indexPath];
    PWADemoCartItem *cartItem = [[PWADemoCart sharedCart] getCartItemAtIndex:indexPath.row];
    
    [cell setDisplayWithCartItem:cartItem];
    [cell.incrementQuantityButton addTarget:self action:@selector(updateView) forControlEvents:UIControlEventTouchUpInside];
    [cell.decrementQuantityButton addTarget:self action:@selector(updateView) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.itemsInCartView.frame.size.height * kProductTableCellHeightMultiplier;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *removeAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal
                                                                            title:@"Remove"
                                                                          handler: ^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                                                                              [[PWADemoCart sharedCart] removeItemAtIndex:indexPath.row];
                                                                              self.checkoutButton.userInteractionEnabled = [[PWADemoCart sharedCart] itemCount] > 0;
                                                                              [self updateView];
                                                                          }];
    removeAction.backgroundColor = [UIColor lightOrangeColor];
    return @[removeAction];
}

#pragma mark - Login with Amazon

- (IBAction)loginAndPayWithAmazon:(id)sender
{
    [self.activityIndicator beginWaitActivity];
    
    PWADemoLoginHandler completionHandler = [PWADemoLoginWithAmazonUtils buildLoginHandlerWithHandler:^(AMZNAuthorizeResult *result, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                [self.activityIndicator endWaitActivity];
            } else {
                [self createAmazonOrderReferenceObject];
            }
        });
    }];
    
    [PWADemoLoginWithAmazonUtils loginWithAmazonWithCompletionHandler:completionHandler];
}

#pragma mark - Pay with Amazon

- (IBAction)checkout:(id)sender
{
    if ([PWADemoAmazonAccount sharedInstance].loggedIn == YES) {
        [self createAmazonOrderReferenceObject];
    } else {
        [self showPaymentsOptionPopUp];
    }
}

- (void)createAmazonOrderReferenceObject
{
    [self.activityIndicator beginWaitActivity];
    
    PWADemoCreateOrderHandler completionHandler = [PWADemoPayWithAmazonUtils buildCreateOrderHandlerWithHandler:^(NSString *amazonOrderReferenceId, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                [self.activityIndicator endWaitActivity];
                [self viewDidAppear:NO];
            } else {
                [self getOrderReferenceDetailsWithId:amazonOrderReferenceId];
            }
        });
    }];
        
    [PWADemoPayWithAmazonUtils createOrderReferenceWithCompletionHandler:completionHandler];
}

- (void)getOrderReferenceDetailsWithId:(NSString *)amazonOrderReferenceId
{
    PWADemoGetDetailsHandler getOrderDetailsHandler = [PWADemoPayWithAmazonUtils buildGetDetailsHandlerWithParsedDetailsHandler: ^(NSDictionary *orderDetails, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                self.order = [[PWADemoOrderReference alloc] initWithAmazonOrderReferenceId:amazonOrderReferenceId];
                [self.order setOrderDetailsWithDictionary:orderDetails];
                
                if (![PWADemoAlertMessageUtils alertMessageIsSet]) {
                    [self performSegueWithIdentifier:kGoToReviewOrderPageSegue sender:self];
                }
            }
            
            [self.activityIndicator endWaitActivity];
            [self viewDidAppear:NO];
        });
    }];
    
    [PWADemoPayWithAmazonUtils getOrderReferenceDetailsForOrderId:amazonOrderReferenceId
                                                completionHandler:getOrderDetailsHandler];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kGoToReviewOrderPageSegue]) {
        // Pass the order to the next scene
        PWADemoReviewOrderViewController *destinationVC = [segue destinationViewController];
        destinationVC.order = self.order;
    }
}

#pragma mark - Pop Up

- (IBAction)hidePaymentOptionPopUp:(id)sender
{
    [UIView animateWithDuration:kPaymentsPopUpAnimationDuration animations: ^{
        [self.paymentPopUpView setCenter:CGPointMake(self.view.frame.size.width * kHidePaymentsPopUpHorizontalMultiplier, self.paymentPopUpView.center.y)];
        self.dimViewButton.alpha = kDimViewInvisibleAlpha;
    }];
    
    self.dimViewButton.hidden = YES;
}

- (void)showPaymentsOptionPopUp
{
    self.dimViewButton.hidden = NO;
    
    [UIView animateWithDuration:kPaymentsPopUpAnimationDuration animations: ^{
        [self.paymentPopUpView setCenter:CGPointMake(self.view.center.x, self.paymentPopUpView.center.y)];
        self.dimViewButton.alpha = kDimViewVisibleAlpha;
    }];
}

@end
