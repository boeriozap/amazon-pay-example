//
//  PWADemoProductListViewController.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoProductListViewController.h"
#import "PWADemoProduct.h"
#import "PWADemoAmazonAccount.h"
#import "PWADemoProductDetailsViewController.h"
#import "UIBarButtonItem+PWADemoCartIcon.h"
#import "PWADemoProductCollectionViewCell.h"
#import "UIViewController+PWADemoAlertMessage.h"
#import "PWADemoLoginWithAmazonUtils.h"
#import "PWADemoAlertMessageUtils.h"
#import <LoginWithAmazon/LoginWithAmazon.h>

/**
 *  The PWADemoProductListViewController controls the Product List scene.
 *  The user arrives at this scene via the Login scene or Thank You scene.
 *  In this scene, the user can scroll through the list of products available for purchase. When a desired product
 *  is found, the user can press it to move to the Product Details scene.
 *  The user is able to view their cart in the Cart scene by pressing the cart icon in the top right corner.
 *  The user is able to login or logout by pressing the login/logout button in the top left corner.
 */
@interface PWADemoProductListViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginLogoutBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cartBarButtonItem;
@property (weak, nonatomic) IBOutlet UICollectionView *productCollectionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property NSString *alertMessage;

/**
 *  Makes sure the view is updated before the view appears.
 */
- (void)viewWillAppear:(BOOL)animated;

/**
 *  Displays an alert message if one is necessary
 */
- (void)viewDidAppear:(BOOL)animated;

/**
 *  Updates the login/logout button's title and the cart bar button item's quantity
 */
- (void)updateView;

/**
 *  Returns the number of preset products
 */
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;

/**
 *  Returns the cell at the given indexPath after setting the cell to display its assigned product
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;

/**
 *  Returns the dimensions of a collection view cell
 */
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

/**
 *  Reverses the login state.
 *  If user is logged in, logs user out. If user is logged out, starts Login With Amazon flow.
 */
- (IBAction)toggleLoginLogoutState:(id)sender;

/**
 *  The user will Login with Amazon. On success, user info is used in integration with merchant account system.
 *  On failure, an alert message is displayed.
 */
- (void)loginWithAmazon;

/**
 *  The user will Logout of Amazon. On success, user is logged out of merchant account system.
 *  On failure, an alert message is displayed.
 */
- (void)logoutOfAmazon;

/**
 *  Once a product button has been clicked, segue to the product details page.
 */
- (void)selectProduct:(UIButton *)sender;

/**
 *  Transfers the product contained in the selected product cell to the product detail view controller.
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

@end

@implementation PWADemoProductListViewController

/**
 *  Constants used for formatting the product list scroll view
 */
const CGFloat kProductCollectionCellWidthToHeightMultiplier = 1.3;
const CGFloat kProductCollectionMarginSize = 4;
const CGFloat kProductCollectionColumnCount = 2;
const CGFloat kProductCollectionMarginCount = kProductCollectionColumnCount + 1;

NSString *const kGoToProductPageSegue = @"goToProductPage";
NSString *const kProductCollectionCell = @"productCollectionCell";

#pragma mark - Display

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateView];
    
    [self.activityIndicator setColor:[UIColor grayColor]];
}

- (void)viewDidAppear:(BOOL)animated
{
    if ([PWADemoAlertMessageUtils alertMessageIsSet]) {
        [self raiseAlertMessage:[PWADemoAlertMessageUtils getAndClearAlertMessage]];
    }
}

- (void)updateView
{
    // Set the login/logout button's text based on current login state
    self.loginLogoutBarButtonItem.title = [PWADemoAmazonAccount sharedInstance].loggedIn ? @"Logout" : @"Login";
    
    [self.cartBarButtonItem updateCartQuantity];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [PWADemoProduct presetProductCount];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PWADemoProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kProductCollectionCell forIndexPath:indexPath];
    [cell setDisplayWithProduct:[[PWADemoProduct alloc] initWithPresetProductNumber:indexPath.row]];
    [cell.productSelectButton addTarget:self action:@selector(selectProduct:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    const CGFloat kProductCollectionCellWidth = (self.view.frame.size.width - (kProductCollectionMarginSize * kProductCollectionMarginCount)) / kProductCollectionColumnCount;
    const CGFloat kProductCollectionCellHeight = kProductCollectionCellWidth * kProductCollectionCellWidthToHeightMultiplier;
    return CGSizeMake(kProductCollectionCellWidth, kProductCollectionCellHeight);
}

#pragma mark - Login With Amazon

- (IBAction)toggleLoginLogoutState:(id)sender
{
    if ([PWADemoAmazonAccount sharedInstance].loggedIn) {
        [self logoutOfAmazon];
    } else {
        [self loginWithAmazon];
    }
}

- (void)loginWithAmazon
{
    [self.activityIndicator beginWaitActivity];
    
    PWADemoLoginHandler completionHandler = [PWADemoLoginWithAmazonUtils buildLoginHandlerWithHandler:^(AMZNAuthorizeResult *result, NSError *error) {
        [self updateView];
        [self.activityIndicator endWaitActivity];
    }];
    
    [PWADemoLoginWithAmazonUtils loginWithAmazonWithCompletionHandler:completionHandler];
}

- (void)logoutOfAmazon
{
    [self.activityIndicator beginWaitActivity];
    
    PWADemoLogoutHandler completionHandler = [PWADemoLoginWithAmazonUtils buildLogoutHandlerWithHandler:^(NSError *error) {
        [self updateView];
        [self.activityIndicator endWaitActivity];
    }];
    
    [PWADemoLoginWithAmazonUtils logoutOfAmazonWithCompletionHandler:completionHandler];
}

#pragma mark - Navigation

- (void)selectProduct:(UIButton *)sender
{
    [self performSegueWithIdentifier:kGoToProductPageSegue sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kGoToProductPageSegue]) {
        // Determine which cell was clicked
        CGPoint cellLocation = [sender convertPoint:CGPointZero toView:self.productCollectionView];
        NSIndexPath *indexPath = [self.productCollectionView indexPathForItemAtPoint:cellLocation];
        UICollectionViewCell *selectedCell = [self collectionView:self.productCollectionView cellForItemAtIndexPath:indexPath];
        
        // Pass the product from the selected sell to the next scene
        PWADemoProductDetailsViewController *destinationVC = [segue destinationViewController];
        destinationVC.selectedProduct = ((PWADemoProductCollectionViewCell *)selectedCell).product;
    }
}

@end
