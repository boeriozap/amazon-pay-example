//
//  UIBarButtonItem+PWADemoCartIcon.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "UIBarButtonItem+PWADemoCartIcon.h"

@implementation UIBarButtonItem (PWADemoCartIcon)

const int kGreatestSingleDigitNumber = 9;
const double kSingleDigitHorizontalPlacementMultiplier = 0.35;
const double kDoubleDigitHorizontalPlacementMultiplier = 0.45;
const int kNumberFontSize = 14;

- (void)updateCartQuantity
{
    UIImage *cartImage = [UIImage imageNamed:@"ShoppingCartIcon.png"];
    
    UIGraphicsBeginImageContext(cartImage.size);
    [cartImage drawInRect:CGRectMake(0, 0, cartImage.size.width, cartImage.size.height)];
    
    // Only display the number if there are products in the cart
    if ([[PWADemoCart sharedCart] itemCount] > 0) {
        CGRect rect;
        // Adjust text location based on the number of digits in the number
        if ([[PWADemoCart sharedCart] itemCount] > kGreatestSingleDigitNumber) {
            rect = CGRectMake(cartImage.size.width * kSingleDigitHorizontalPlacementMultiplier, 0, cartImage.size.width, cartImage.size.height);
        } else {
            rect = CGRectMake(cartImage.size.width * kDoubleDigitHorizontalPlacementMultiplier, 0, cartImage.size.width, cartImage.size.height);
        }
        
        // Set text and its attributes
        NSString *distinctItemCount = [NSString stringWithFormat:@"%ld", (long)[[PWADemoCart sharedCart] itemCount]];
        NSDictionary *textAttributes = @{ NSFontAttributeName:[UIFont boldSystemFontOfSize:kNumberFontSize] };
        [distinctItemCount drawInRect:rect withAttributes:textAttributes];
    }
    
    cartImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self setImage:cartImage];
}

@end
