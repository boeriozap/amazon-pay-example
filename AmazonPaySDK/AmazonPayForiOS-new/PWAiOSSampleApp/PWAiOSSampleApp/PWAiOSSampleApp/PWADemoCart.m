//
//  PWADemoCart.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoCart.h"
#import "PWADemoProduct.h"

@interface PWADemoCart ()

/**
 *  An array of cart items
 */
@property NSMutableArray<PWADemoCartItem *> *itemsInTheCart;

@end

@implementation PWADemoCart

const double kTaxRate = 0.075;
const double kShippingCost = 10;
const double kPriceToGetFreeShipping = 50;

+ (PWADemoCart *)sharedCart
{
    static PWADemoCart *sharedCart = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedCart = [[self alloc] init];
    });
    return sharedCart;
}

- (instancetype)init
{
    self.itemsInTheCart = [[NSMutableArray alloc] init];
    return self;
}

#pragma mark - Cart Contents

- (NSUInteger)itemCount
{
    return self.itemsInTheCart.count;
}

- (BOOL)itemIsInCart:(PWADemoProduct *)product
{
    PWADemoCartItem *cartItem = [[PWADemoCartItem alloc] initWithProduct:product quantity:0];
    return [self.itemsInTheCart indexOfObject:cartItem] != NSNotFound;
}

- (PWADemoCartItem *)getCartItemAtIndex:(NSUInteger)cartIndex
{
    return [self.itemsInTheCart objectAtIndex:cartIndex];
}

- (void)addItemToCart:(PWADemoProduct *)productToAdd withQuantity:(NSUInteger)quantityToAdd
{
    PWADemoCartItem *cartItemToAdd = [[PWADemoCartItem alloc] initWithProduct:productToAdd quantity:quantityToAdd];
    if ([self.itemsInTheCart indexOfObject:cartItemToAdd] == NSNotFound) {
        [self.itemsInTheCart addObject:cartItemToAdd];
    } else {
        NSInteger cartIndex = [self.itemsInTheCart indexOfObject:cartItemToAdd];
        [[self.itemsInTheCart objectAtIndex:cartIndex] addQuantity:quantityToAdd];
    }
}

- (void)subtractItemFromCart:(PWADemoProduct *)productToSubtract
{
    PWADemoCartItem *cartItemToSubtract = [[PWADemoCartItem alloc] initWithProduct:productToSubtract quantity:0];
    NSInteger cartIndex = [self.itemsInTheCart indexOfObject:cartItemToSubtract];
    [[self.itemsInTheCart objectAtIndex:cartIndex] subtractQuantityByOne];
    if ([self.itemsInTheCart objectAtIndex:cartIndex].quantity == 0) {
        [self removeItemAtIndex:cartIndex];
    }
}

- (void)removeItemAtIndex:(NSUInteger)cartIndex
{
    [self.itemsInTheCart removeObjectAtIndex:cartIndex];
}

- (void)emptyCart
{
    [self.itemsInTheCart removeAllObjects];
}

#pragma mark - Cost

- (NSDecimalNumber *)getTotalCost
{
    NSDecimalNumber *totalCost = [NSDecimalNumber zero];
    
    for (NSInteger cartIndex = 0; cartIndex < [self itemCount]; cartIndex++) {
        PWADemoCartItem *cartItem = [self getCartItemAtIndex:cartIndex];
        totalCost = [totalCost decimalNumberByAdding:[[[NSDecimalNumber alloc] initWithInt:(int)(cartItem.quantity)] decimalNumberByMultiplyingBy:cartItem.product.unitPrice]];
    }
    
    return totalCost;
}

- (NSDecimalNumber *)getShippingCost
{
    if ([self itemCount] == 0 || [self qualifiesForFreeShipping]) {
        return [NSDecimalNumber zero];
    } else {
        return [[NSDecimalNumber alloc] initWithDouble:kShippingCost];
    }
}

- (BOOL)qualifiesForFreeShipping
{
    return [[self getTotalCost] compare:[[NSDecimalNumber alloc] initWithDouble:kPriceToGetFreeShipping]] == NSOrderedDescending;
}

- (NSDecimalNumber *)getTaxAmount
{
    return [[[self getTotalCost] decimalNumberByAdding:[self getShippingCost]] decimalNumberByMultiplyingBy:[[NSDecimalNumber alloc] initWithDouble:kTaxRate]];
}

- (NSDecimalNumber *)getTotalCostWithShippingAndTax
{
    
    return [[[self getTotalCost] decimalNumberByAdding:[self getShippingCost]] decimalNumberByAdding:[self getTaxAmount]];
}

@end
