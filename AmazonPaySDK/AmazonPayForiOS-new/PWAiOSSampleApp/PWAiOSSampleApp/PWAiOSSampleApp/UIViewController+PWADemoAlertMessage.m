//
//  UIViewController+PWADemoAlertMessage.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "UIViewController+PWADemoAlertMessage.h"

@implementation UIViewController (PWADemoAlertMessage)

- (void)raiseAlertMessage:(NSString *)alertMessage
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertMessage
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
