//
//  PWADemoLoginViewController.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoLoginViewController.h"
#import "PWADemoAmazonAccount.h"
#import "UIViewController+PWADemoAlertMessage.h"
#import "PWADemoLoginWithAmazonUtils.h"
#import "PWADemoAlertMessageUtils.h"
#import <LoginWithAmazon/LoginWithAmazon.h>

/**
 *  The PWADemoLoginViewController controls the Login scene.
 *  The user arrives at this scene via the Select Region scene.
 *  During this scene the user can decide to login to the app with Amazon, or browse the app as a guest.
 *  After making their choice, the user is sent to the Product List scene to begin shopping.
 */
@interface PWADemoLoginViewController ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property NSString *alertMessage;

/**
 *  When the authorize call is made from the JP Merchant in the FE region, the SFSafariViewController lingers after
 *  Login with Amazon has completed. Due to this, when the segue to the product list page is attempted in the completion
 *  handler, we get a warning stating that the SFSafariViewController is not in the window hierarchy, and thus the normal
 *  segue to the product list page cannot occur. To workaround this issue in the JP Merchant implementation, we set the
 *  didLogin flag upon successful login and perform the segue in the viewDidAppear method.
 */
#if JP_MERCHANT
@property BOOL didLogin;
#endif

/**
 *  Sets the activity indicator's color
 */
- (void)viewWillAppear:(BOOL)animated;

/**
 *  Displays an alert message if one is necessary
 */
- (void)viewDidAppear:(BOOL)animated;

/**
 *  User chooses to browse as guest. Set logged in flag to NO
 */
- (IBAction)browseAsAGuest:(id)sender;

/**
 *  User chooses Login With Amazon. Set logged in flag to YES and begin Login with Amazon flow.
 *  If successful, segue to product list page.
 */
- (IBAction)loginWithAmazon:(id)sender;

@end

@implementation PWADemoLoginViewController

NSString *const kGoToProductListPageSeque = @"goToProductListPage";

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.activityIndicator setColor:[UIColor grayColor]];
}

- (void)viewDidAppear:(BOOL)animated
{
    if ([PWADemoAlertMessageUtils alertMessageIsSet]) {
        [self raiseAlertMessage:[PWADemoAlertMessageUtils getAndClearAlertMessage]];
    }
    
// See comment above
#if JP_MERCHANT
    if (self.didLogin) {
        [self performSegueWithIdentifier:kGoToProductListPageSeque sender:self];
    }
#endif
}

- (IBAction)browseAsAGuest:(id)sender
{
    [[PWADemoAmazonAccount sharedInstance] setLoggedIn:NO];
}

#pragma mark - Login With Amazon

- (IBAction)loginWithAmazon:(id)sender
{
// See comment above
#if JP_MERCHANT
    self.didLogin = NO;
#endif
    
    [self.activityIndicator beginWaitActivity];
    
    PWADemoLoginHandler completionHandler = [PWADemoLoginWithAmazonUtils buildLoginHandlerWithHandler:^(AMZNAuthorizeResult *result, NSError *error) {
        if (!error) {
// See comment above
#if JP_MERCHANT
            self.didLogin = YES;
#endif
            [self performSegueWithIdentifier:kGoToProductListPageSeque sender:self];
        }
        [self.activityIndicator endWaitActivity];
    }];
    
    [PWADemoLoginWithAmazonUtils loginWithAmazonWithCompletionHandler:completionHandler];
}

@end
