//
//  PWADemoProductCollectionViewCell.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoProductCollectionViewCell.h"
#import "PWADemoProduct.h"
#import "UIColor+PWADemoColors.h"
#import "PWADemoCurrencyFormatter.h"

@interface PWADemoProductCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property UIView *highlightedView;

/**
 *  Formats the background and borders of the cell
 */
- (void)formatBackground;

/**
 *  Initializes the button with actions for highlighting and unhighlighting
 */
- (void)initializeProductSelectButton;

/**
 *  Initializes the higlight view by setting color and hiding it
 */
- (void)initializeHighlightView;

/**
 *  Highlights the cell by displaying the highlight view
 */
- (void)highlightCell;

/**
 *  Unhighlights the cell by hiding the highlight view
 */
- (void)unhighlightCell;

@end

@implementation PWADemoProductCollectionViewCell

/**
 *  Constants used in formatting
 */
const int kCellCornerRadius = 5;
const int kCellBorderWidth = 1;
const double kHighlightTransperancy = 0.3;

static UIColor *highlightBackgroundColor;
static struct CGColor *highlightBorderColor;
static UIColor *normalBackgroundColor;
static struct CGColor *normalBorderColor;

+ (void)initialize
{
    highlightBackgroundColor = [UIColor torquoiseColor];
    highlightBorderColor = highlightBackgroundColor.CGColor;
    normalBackgroundColor = [UIColor whiteColor];
    normalBorderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setDisplayWithProduct:(PWADemoProduct *)product
{
    _product = product;
    
    [self formatBackground];
    
    self.productImageView.image = [UIImage imageNamed:self.product.productImageName];
    self.productNameLabel.text = self.product.productName;
    self.productPriceLabel.text = [PWADemoCurrencyFormatter stringFromNumber:self.product.unitPrice];
    
    [self initializeProductSelectButton];
    [self initializeHighlightView];
}

- (void)formatBackground
{
    self.contentView.backgroundColor = normalBackgroundColor;
    self.contentView.layer.cornerRadius = kCellCornerRadius;
    self.contentView.layer.borderColor = normalBorderColor;
    self.contentView.layer.borderWidth = kCellBorderWidth;
    self.contentView.layer.masksToBounds = YES;
}

- (void)initializeProductSelectButton
{
    self.productSelectButton = [[UIButton alloc] initWithFrame:self.contentView.frame];
    
    [self.productSelectButton addTarget:self action:@selector(highlightCell) forControlEvents:UIControlEventTouchDown];
    [self.productSelectButton addTarget:self action:@selector(unhighlightCell) forControlEvents:UIControlEventTouchUpInside];
    [self.productSelectButton addTarget:self action:@selector(unhighlightCell) forControlEvents:UIControlEventTouchUpOutside];
    [self.productSelectButton addTarget:self action:@selector(unhighlightCell) forControlEvents:UIControlEventTouchCancel];
    
    [self.contentView addSubview:self.productSelectButton];
}

- (void)initializeHighlightView
{
    self.highlightedView = [[UIView alloc] initWithFrame:self.contentView.frame];
    self.highlightedView.backgroundColor = highlightBackgroundColor;
    self.highlightedView.alpha = kHighlightTransperancy;
    self.highlightedView.hidden = YES;
    
    [self.contentView addSubview:self.highlightedView];
}

- (void)highlightCell
{
    self.highlightedView.hidden = NO;
    self.contentView.layer.borderColor = highlightBorderColor;
}

- (void)unhighlightCell
{
    self.highlightedView.hidden = YES;
    self.contentView.layer.borderColor = normalBorderColor;
}

@end
