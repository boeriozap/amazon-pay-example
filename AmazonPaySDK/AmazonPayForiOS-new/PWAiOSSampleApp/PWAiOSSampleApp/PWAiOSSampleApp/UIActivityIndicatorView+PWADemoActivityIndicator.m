//
//  UIActivityIndicatorView+PWADemoActivityIndicator.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "UIActivityIndicatorView+PWADemoActivityIndicator.h"

@implementation UIActivityIndicatorView (PWADemoActivityIndicator)

- (void)beginWaitActivity
{
    if (![[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
    [self startAnimating];
}

- (void)endWaitActivity
{
    [self stopAnimating];
    if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
}

@end
