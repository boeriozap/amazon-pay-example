//
//  SampleBackendGetOrderReferenceDetailsRequest.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SampleBackendGetRequest.h"

@interface SampleBackendGetOrderReferenceDetailsRequest :SampleBackendGetRequest

extern NSString *const kGetOrderReferenceDetailsResultKey;
extern NSString *const kOrderReferenceDetailsKey;
extern NSString *const kOrderReferenceStatusKey;
extern NSString *const kStateKey;

@property (nonatomic, copy) NSString *amazonOrderReferenceID;
@property (nonatomic, copy) NSString *accessToken;

- (instancetype)initWithAmazonOrderReferenceID:(NSString *)amazonOrderReferenceID accessToken:(NSString *)accessToken;

@end
