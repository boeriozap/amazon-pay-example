//
//  PWADemoXMLParser.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoXMLParser.h"

@interface PWADemoXMLParser ()

@property (nonatomic, strong) NSMutableString *currentElement;
@property (nonatomic, strong) NSMutableArray<NSString *> *path;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSString *> *data;
@property (copy, nonatomic) void (^onParseCompletion)(NSDictionary *data, NSError *error);

@end

@implementation PWADemoXMLParser

- (instancetype)initWithCompleteHandler: (void (^)(NSDictionary *, NSError *))onParseCompletion
{
    self.currentElement = nil;
    self.path = [[NSMutableArray alloc] init];
    self.data = [[NSMutableDictionary alloc] init];
    self.onParseCompletion = onParseCompletion;
    
    return [self init];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    [self.path addObject:elementName];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    // Ignore if all characters are whitespace
    if(![[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqual: @"\n"]){
        if (!self.currentElement) {
            // If current element has no characters, current element is set to string
            self.currentElement = [NSMutableString stringWithString:string];
        } else {
            // If current element already has characters, append string to current element
            [self.currentElement appendString:string];
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (self.currentElement) {
        NSArray *lastTwoObjects = [self.path subarrayWithRange:NSMakeRange([self.path count] - 2, 2)];
        NSString *key = [lastTwoObjects componentsJoinedByString:@"."];
        [self.data setObject:self.currentElement forKey:key];
    }
    
    [self.path removeObject:elementName];
    self.currentElement = nil;
}

- (void) parserDidEndDocument:(NSXMLParser *)parser
{
    self.onParseCompletion(self.data, nil);
}

@end
