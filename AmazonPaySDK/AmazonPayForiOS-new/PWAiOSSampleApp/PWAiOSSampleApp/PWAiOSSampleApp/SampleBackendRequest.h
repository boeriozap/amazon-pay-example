//
//  SampleBackendRequest.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SampleBackendRequest : NSObject

@property (nonatomic, copy) NSString *host;
@property (nonatomic, copy) NSString *path;

- (NSURLRequest *)generateRequest;
- (NSDictionary *)requestQueryItems;
- (BOOL) isResponseValid:(NSDictionary *)response error:(NSError **)error;
- (void) sendRequestWithCompletionHandler:(void (^)(NSDictionary *jsonDictionary, NSError *error))completionHandler;

@end
