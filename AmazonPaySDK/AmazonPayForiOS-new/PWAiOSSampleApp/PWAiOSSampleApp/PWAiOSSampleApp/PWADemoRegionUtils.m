//
//  PWADemoRegionUtils.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoRegionUtils.h"

@implementation PWADemoRegionUtils

NSString *const kNA = @"NA";
NSString *const kEU = @"EU";
NSString *const kFE = @"FE";

+ (PWARegion)pwaRegionFromString:(NSString *)regionString
{
    if ([regionString isEqualToString:kNA]) {
        return NA;
    } else if ([regionString isEqualToString:kEU]) {
        return EU;
    } else if ([regionString isEqualToString:kFE]) {
        return FE;
    } else {
        [NSException raise:NSInvalidArgumentException format:@"PWA region not defined for region string %@", regionString];
    }
    return NSIntegerMin;
}

+ (NSString *)stringFromPWARegion:(PWARegion)region
{
    switch (region) {
        case NA:
            return kNA;
        case EU:
            return kEU;
        case FE:
            return kFE;
        default:
            [NSException raise:NSInvalidArgumentException format:@"String value not defined for PWA region %ld", (long)region];
    }
    return nil;
}

+ (AMZNRegion)lwaRegionFromString:(NSString *)regionString
{
    if ([regionString isEqualToString:kNA]) {
        return AMZNRegionNA;
    } else if ([regionString isEqualToString:kEU]) {
        return AMZNRegionEU;
    } else if ([regionString isEqualToString:kFE]) {
        return AMZNRegionFE;
    } else {
        [NSException raise:NSInvalidArgumentException format:@"LWA region not defined for region string %@", regionString];
    }
    return NSIntegerMin;
}

+ (NSString *)stringFromLWARegion:(AMZNRegion)region
{
    switch (region) {
        case AMZNRegionNA:
            return kNA;
        case AMZNRegionEU:
            return kEU;
        case AMZNRegionFE:
            return kFE;
        default:
            [NSException raise:NSInvalidArgumentException format:@"String value not defined for LWA region %ld", (long)region];
    }
    return nil;
}

@end
