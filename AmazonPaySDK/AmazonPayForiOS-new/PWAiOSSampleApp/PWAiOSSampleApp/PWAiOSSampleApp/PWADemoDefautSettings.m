//
//  PWADemoDefautSettings.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoDefaultSettings.h"

@implementation PWADemoDefaultSettings

NSString *const kPWADemoDefaultSettings = @"PWADemoDefaultSettings";
NSString *const kLWARegion = @"LWARegion";
NSString *const kPWARegion = @"PWARegion";
NSString *const kLedgerCurrency = @"LedgerCurrency";
NSString *const kLocaleIdentifier = @"LocaleIdentifier";

+ (instancetype)sharedSettings
{
    static PWADemoDefaultSettings *sharedSettings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSettings = [[self alloc] init];
    });
    return sharedSettings;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _defaultLWARegion = [[NSBundle mainBundle] infoDictionary][kPWADemoDefaultSettings][kLWARegion];
        _defaultPWARegion = [[NSBundle mainBundle] infoDictionary][kPWADemoDefaultSettings][kPWARegion];
        _defaultLedgerCurrency = [[NSBundle mainBundle] infoDictionary][kPWADemoDefaultSettings][kLedgerCurrency];
        _defaultLocaleIdentifier = [[NSBundle mainBundle] infoDictionary][kPWADemoDefaultSettings][kLocaleIdentifier];
    }
    return self;
}

- (void)setDefaultLWARegion:(NSString *)defaultLWARegion
{
    _defaultLWARegion = defaultLWARegion;
}

- (void)setDefaultPWARegion:(NSString *)defaultPWARegion
{
     _defaultPWARegion = defaultPWARegion;
}

- (void)setDefaultLedgerCurrency:(NSString *)defaultLedgerCurrency
{
    _defaultLedgerCurrency = defaultLedgerCurrency;
}

- (void)setDefaultLocaleIdentifier:(NSString *)defaultLocaleIdentifier
{
    _defaultLocaleIdentifier = defaultLocaleIdentifier;
}

@end