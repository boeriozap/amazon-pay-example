//
//  UIActivityIndicatorView+PWADemoActivityIndicator.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActivityIndicatorView (PWADemoActivityIndicator)

- (void)beginWaitActivity;

- (void)endWaitActivity;

@end
