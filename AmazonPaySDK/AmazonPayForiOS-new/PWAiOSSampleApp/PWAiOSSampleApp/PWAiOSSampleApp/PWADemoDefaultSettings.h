//
//  PWADemoDefaultSettings.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWADemoDefaultSettings : NSObject

@property (readonly) NSString *defaultLWARegion;
@property (readonly) NSString *defaultPWARegion;
@property (readonly) NSString *defaultLedgerCurrency;
@property (readonly) NSString *defaultLocaleIdentifier;

+ (instancetype)sharedSettings;

- (void)setDefaultLWARegion:(NSString *)defaultLWARegion;

- (void)setDefaultPWARegion:(NSString *)defaultPWARegion;

- (void)setDefaultLedgerCurrency:(NSString *)defaultLedgerCurrency;

- (void)setDefaultLocaleIdentifier:(NSString *)defaultLocaleIdentifier;

@end
