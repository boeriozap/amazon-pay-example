//
//  PWADemoProduct.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  The PWADemoProduct class contains the product information and methods to alter that information,
 *  as well as a list of preset eCommerce products.
 */
@interface PWADemoProduct : NSObject

/**
 *  The name of the product
 */
@property (readonly) NSString *productName;

/**
 *  The name of the product's picture
 */
@property (readonly) NSString *productImageName;

/**
 *  The color of the product
 */
@property (readonly) NSString *productColor;

/**
 *  The price of one unit of the product
 */
@property (readonly) NSDecimalNumber *unitPrice;

/**
 *  Initialization of product by assigning the default product values to its properties
 */
- (instancetype)init;

/**
 *  Initialization of product by copying the properties of another product
 */
- (instancetype)initWithProduct:(PWADemoProduct *)product;

/**
 *  Initialization of product by assigning values to each property with individual parameters
 */
- (instancetype)initWithProductName:(NSString *)name image:(NSString *)image color:(NSString *)color price:(NSDecimalNumber *)price;

/**
 *  Initializes a product to one of the preset products
 *  @param productNumber    Indicates which preset product to use. If greater than number of preset products, then the last preset product is used
 *  @return                 The initialized product
 */
- (instancetype)initWithPresetProductNumber:(NSUInteger)productNumber;

/**
 *  Checks if products have idential product names
 */
- (BOOL)isEqual:(id)object;

/**
 *  Returns the number of preset eCommerce products
 */
+ (NSInteger)presetProductCount;

@end
