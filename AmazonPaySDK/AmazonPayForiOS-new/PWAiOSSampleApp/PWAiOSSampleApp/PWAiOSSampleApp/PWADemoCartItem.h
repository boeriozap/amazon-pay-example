//
//  PWADemoCartItem.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PWADemoProduct.h"

/**
 *  The PWADemoCartItem class contains the information about an item the cart
 */
@interface PWADemoCartItem : NSObject

/**
 *  The product in the cart
 */
@property (readonly) PWADemoProduct *product;

/**
 *  The quantity of the product that has been selected for purchase
 */
@property (readonly) NSUInteger quantity;

/**
 *  Cart item initialization
 */
- (instancetype)initWithProduct:(PWADemoProduct *)product quantity:(NSUInteger)quantity;

/**
 *  Checks if the products of the cart items have identical product names
 */
- (BOOL)isEqual:(id)object;

/**
 *  Increases the quantity by the amount given
 */
- (void)addQuantity:(NSUInteger)quantityToAdd;

/**
 *  Decreases the quantity by one
 */
- (void)subtractQuantityByOne;

/**
 *  Updates the quantity to the amount given
 */
- (void)updateQuantity:(NSUInteger)newQuantity;

@end

