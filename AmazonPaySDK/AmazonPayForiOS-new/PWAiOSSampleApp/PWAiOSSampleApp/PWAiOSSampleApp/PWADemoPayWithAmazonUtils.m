//
//  PWADemoPayWithAmazonUtils.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoPayWithAmazonUtils.h"
#import "UIActivityIndicatorView+PWADemoActivityIndicator.h"
#import "PWADemoAlertMessageUtils.h"
#import "PWADemoXMLParser.h"
#import "SampleBackendServer.h"
#import "PWADemoDefaultSettings.h"
#import "SampleBackendGenerateSignatureRequest.h"
#import "SampleBackendGetOrderReferenceDetailsRequest.h"
#import "SampleBackendServiceClient.h"

@implementation PWADemoPayWithAmazonUtils

NSString *const kAWSAccessKeyId = @"AWSAccessKeyId";

#pragma mark - Create Order Reference

+ (void)createOrderReferenceWithCompletionHandler:(PWADemoCreateOrderHandler)completionHandler
{
    // Construct the request object with the mandatory parameter useAmazonAddressBook;
    // Set to true if the shipping address required, otherwise set to false
    APayCreateOrderReferenceRequest *createOrderReferenceRequest = [[APayCreateOrderReferenceRequest alloc] initWithUseAmazonAddressBook:YES];
    
    // Optional. set this only if you want to override the region for this order.
    //createOrderReferenceRequest.region = overrideRegion;
    
    // Optional. Set this only if you want to override the ledger currency for this order.
    //createOrderReferenceRequest.ledgerCurrencyCode = overrideLedgerCurrencyCode;
    
    // Optional. Set this only if you want to override the locale for this order.
    //createOrderReferenceRequest.locale = overrideLocale;
    
    NSLog(@"Making Create Order Reference AmazonPay API call");
    [AmazonPay createOrderReference:createOrderReferenceRequest
                      completionHandler:completionHandler];
}

+ (PWADemoCreateOrderHandler)buildCreateOrderHandlerWithHandler:(void (^)(NSString *, NSError *))supplementaryHandler
{
    return ^(NSString *amazonOrderReferenceId, NSError *error) {
        if (error) {
            NSLog(@"Create order reference failed");
            NSLog(@"Error during create order: %@", error.description);
            [PWADemoAlertMessageUtils setAlertMessage:@"Failed to initiate Pay With Amazon"];
        } else {
            NSLog(@"Create order reference succeeded");
            NSLog(@"Amazon order reference Id: %@", amazonOrderReferenceId);
        }
        
        supplementaryHandler(amazonOrderReferenceId, error);
    };
}

#pragma mark - Get Order Reference Details

+ (void)getOrderReferenceDetailsForOrderId:(NSString *)amazonOrderReferenceID completionHandler:(void (^)(NSDictionary *, NSError *))completionHandler
{
    AMZNAuthorizeRequest *request = [[AMZNAuthorizeRequest alloc] init];
    
    request.scopes = [[NSArray alloc] initWithObjects:[APayPaymentScope initiate], nil];
    // The SDK will check for locally cached authorized grant. Even if authorized grant is not found, SignIn flow will not occur
    request.interactiveStrategy = AMZNInteractiveStrategyNever;
    
    NSLog(@"Making Authorize LWA API call for Get Order Details");
    [[AMZNAuthorizationManager sharedManager] authorize:request
                                            withHandler:^(AMZNAuthorizeResult * _Nullable result, BOOL userDidCancel, NSError * _Nullable error) {
                                                if (error) {
                                                    NSLog(@"LWA Authorize error: %@", error.description);
                                                    [PWADemoAlertMessageUtils setAlertMessage:@"Error authorizing with Amazon"];
                                                    return;
                                                }
                                                
                                                NSLog(@"Getting order reference details from merchant backend");
                                                [SampleBackendServer getOrderReferenceDetailsWithToken:result.token
                                                                                    orderReferenceID:amazonOrderReferenceID
                                                                                   completionHandler:completionHandler];
                                            }];
}

+ (PWADemoGetDetailsHandler)buildGetDetailsHandlerWithParsedDetailsHandler:(void (^)(NSDictionary *, NSError*))parsedDetailsHandler
{
    return ^(NSDictionary *jsonDictionary, NSError *error) {
        if (error) {
            NSLog(@"Get Details error: %@", error.description);
            NSLog(@"Error JSON response: %@", jsonDictionary);
            [PWADemoAlertMessageUtils setAlertMessage:@"Error retrieving order details"];
            parsedDetailsHandler(nil, error);
        } else {
            [PWADemoPayWithAmazonUtils parseData:jsonDictionary supplementaryHandler:parsedDetailsHandler];
        }
    };
}

+ (void)parseData:(NSDictionary *)jsonDictionary supplementaryHandler:(void (^)(NSDictionary *, NSError*))supplementaryHandler
{
   
    NSLog(@"Get Details parsed response:\n%@", jsonDictionary);
    supplementaryHandler(jsonDictionary, nil);
}

#pragma mark - Change Order Details

+ (void)changePaymentMethodForOrderId:(NSString *)amazonOrderReferenceId
                    completionHandler:(void (^)(APayChangeOrderDetailsResponse *, NSError *))completionHandler
{
    // Construct the request object with the mandatory parameter
    // amazonOrderReferenceId obtained during createOrderReference.
    APayChangeOrderDetailsRequest *changePaymentMethodRequest = [[APayChangeOrderDetailsRequest alloc] initWithAmazonOrderReferenceID:amazonOrderReferenceId];
    
    NSLog(@"Making Change Payment Method AmazonPay API call");
    [AmazonPay changePaymentMethod:changePaymentMethodRequest
                     completionHandler:completionHandler];
}

+ (void)changeShippingAddressForOrderId:(NSString *)amazonOrderReferenceId
                      completionHandler:(void (^)(APayChangeOrderDetailsResponse *, NSError *))completionHandler
{
    // Construct the request object with the mandatory parameter
    // amazonOrderReferenceId obtained during createOrderReference.
    APayChangeOrderDetailsRequest *changePaymentMethodRequest = [[APayChangeOrderDetailsRequest alloc] initWithAmazonOrderReferenceID:amazonOrderReferenceId];
    
    NSLog(@"Making Change Shipping Address AmazonPay API call");
    [AmazonPay changeShippingAddress:changePaymentMethodRequest
                       completionHandler:completionHandler];
}

+ (PWADemoChangeOrderDetailsHandler)buildChangeOrderDetailsHandlerWithHandler:(PWADemoChangeOrderDetailsHandler)supplementaryHandler
{
    return ^(APayChangeOrderDetailsResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Change order details failed");
            NSLog(@"Error during change order details flow: %@", error.description);
            [PWADemoAlertMessageUtils setAlertMessage:@"Failed to change payment details"];
        } else {
            NSLog(@"Change order details succeeded");
        }
        
        supplementaryHandler(response, error);
    };
}

#pragma mark - Process Payment

+ (void)processPaymentWithPrice:(NSDecimalNumber *)orderPrice
         amazonOrderReferenceId:(NSString *)amazonOrderReferenceId
                  sellerOrderId:(NSString *)sellerOrderId
              completionHandler:(PWADemoProcessPaymentHandler)completionHandler
{
    APayPrice *price = [APayPrice priceWithAmount:orderPrice currencyCode:[PWADemoDefaultSettings sharedSettings].defaultLedgerCurrency];
    
    APayProcessPaymentRequest *request = [[APayProcessPaymentRequest alloc] initWithAmazonOrderReferenceID:amazonOrderReferenceId
                                                                                              orderTotal:price
                                                                                          awsAccessKeyID:[[NSBundle mainBundle] infoDictionary][kAWSAccessKeyId]];
    // Set optional parameters in request object
    // Options: @"Authorize" @"ConfirmOrder" @"AuthorizeAndCapture"
    // See integration guide on details about each option
    request.paymentAction = @"AuthorizeAndCapture";
    
    // Construct and set optional seller order attributes
    APaySellerOrderAttributes *orderAttributes = [[APaySellerOrderAttributes alloc] init];
    orderAttributes.sellerOrderID = sellerOrderId;
    orderAttributes.sellerStoreName = @"MTrend";
    orderAttributes.customInformation = @"Any additional information that merchant would like to include with the order for their reference";
    orderAttributes.sellerNote = @"Thanks for shopping with us!";
    request.sellerOrderAttributes = orderAttributes;
    
    // Construct and set optional authorize attributes
    APayAuthorizeAttributes *authorizeAttributes = [[APayAuthorizeAttributes alloc] init];
    // You can authorize a lesser amount than the order total.
    authorizeAttributes.authorizationAmount = price;
    authorizeAttributes.sellerSoftDescriptor = @"AMZ* MTrend";
    request.authorizeAttributes = authorizeAttributes;
    
    SampleBackendGenerateSignatureRequest *signatureRequest = [[SampleBackendGenerateSignatureRequest alloc] initWithTextToSign:[AmazonPayUtil generateStringToSign:request]];
    [signatureRequest sendRequestWithCompletionHandler:^(NSDictionary *jsonDictionary, NSError *error) {
        if (error) {
            [PWADemoAlertMessageUtils setAlertMessage:@"Failed to generate signature"];
            NSLog(@"Failed to generate signature: %@", error.localizedDescription);
            return;
        }
        
        // Once your request is completely intialized, call your backend to generate a signature
        request.signature = jsonDictionary[@"signature"];
        NSLog(@"Process payment signature: %@", request.signature);
        
        NSLog(@"Making Process Payment PWA API call");
        [AmazonPay processPayment:request
                    completionHandler:completionHandler];
    }];
}

+ (PWADemoProcessPaymentHandler)buildProcessPaymentHandlerWithDetailsHandler:(PWADemoProcessPaymentDetailsHandler)supplementaryDetailsHandler
                                                        supplementaryHandler:(PWADemoProcessPaymentHandler)supplementaryHandler
{
    return ^(APayProcessPaymentResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Process payment failed");
            NSLog(@"Error during process payment: %@", error.description);
            [PWADemoAlertMessageUtils setAlertMessage:@"Failed to process payment"];
        } else if (response) {
            NSLog(@"Process payment response received");
            
            PWADemoGetDetailsHandler detailsHandler = [PWADemoPayWithAmazonUtils buildGetDetailsHandlerWithParsedDetailsHandler:^(NSDictionary *details, NSError *detailsError) {
                // Check if user has canceled the order
                if (![PWADemoPayWithAmazonUtils orderIsOpen:details]) {
                    NSLog(@"Process payment failed");
                    NSLog(@"OrderReferenceStatus state is not Open");
                    [PWADemoAlertMessageUtils setAlertMessage:@"Failed to process payment"];
                } else {
                    NSLog(@"Process payment succeeded");
                    
                    supplementaryDetailsHandler(details, detailsError);
                }
            }];
            
            [PWADemoPayWithAmazonUtils getOrderReferenceDetailsForOrderId:response.amazonOrderReferenceId
                                                    completionHandler:detailsHandler];
        }
        
        supplementaryHandler(response, error);
    };
}

+ (BOOL)orderIsOpen:(NSDictionary *)details
{
    NSString *state = details[kGetOrderReferenceDetailsResultKey][kOrderReferenceDetailsKey][kOrderReferenceStatusKey][kStateKey];
    return (state != nil) && [state isEqualToString:@"Open"];
}

@end
