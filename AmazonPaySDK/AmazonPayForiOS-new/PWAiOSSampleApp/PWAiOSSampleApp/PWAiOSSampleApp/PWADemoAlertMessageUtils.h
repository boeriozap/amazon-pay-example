//
//  PWADemoAmazonIntegrationUtils.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWADemoAlertMessageUtils : NSObject

/**
 *  Indicates whether or not the alert message is set
 */
+ (BOOL)alertMessageIsSet;

/**
 *  Gets and clears the alert message
 *  @return The alert message or nil if there is no message
 */
+ (NSString *)getAndClearAlertMessage;

/**
 *  Sets the alert message to the indicated message
 */
+ (void)setAlertMessage:(NSString *)message;

@end
