//
//  PWADemoProductDetailsViewController.m
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import "PWADemoProductDetailsViewController.h"
#import "PWADemoCart.h"
#import "UIBarButtonItem+PWADemoCartIcon.h"
#import "PWADemoCurrencyFormatter.h"

/**
 *  The PWADemoProductDetailsViewController controls the Product Detail scene.
 *  The user arrives at this scene via the Product List scene.
 *  In this scene, the user is shown their selected product and its details: image, namge, color, price.
 *  They are able to change the quantity of the product they wish to add to their cart by pressing the + and - buttons,
 *  and they are able to add the displayed item to their cart by pressing the add to cart button.
 *  The user is able to view their cart in the Cart scene by pressing the cart icon in the top right corner.
 *  The user is able to navigate back to the Product List scene by pressing the back button in the top left corner.
 */
@interface PWADemoProductDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productColorLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *productQuantityLabel;
@property (weak, nonatomic) IBOutlet UIButton *addToCartButton;
@property (weak, nonatomic) IBOutlet UIButton *incrementQuantityButton;
@property (weak, nonatomic) IBOutlet UIButton *decrementQuantityButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cartBarButtonItem;

/**
 *  The quantity selected of the current product to add to the cart
 */
@property NSUInteger productQuantitySelected;

/**
 *  Flag indicating if the last action made by the user was to add the product to the cart
 */
@property BOOL isAddedToCart;

/**
 *  Initializes the view
 */
- (void)viewWillAppear:(BOOL)animated;

/**
 *  Displays the selected products details
 */
- (void)displayProductDetails;

/**
 *  Displays the buttons' default appearances
 */
- (void)displayButtonsBeforeProductAdded;

/**
 *  Alters the buttons' appearances to reflect that the product has been added to the cart
 */
- (void)displayButtonsAfterProductAdded;

/**
 *  Updates and displays the cart icon quantity, the product quantity, and the buttons
 */
- (void)updateView;

/**
 *  Increments the quantity selected
 */
- (IBAction)incrementQuantity:(id)sender;

/**
 *  Decrements the quantity selected unless that value is one
 */
- (IBAction)decrementQuantity:(id)sender;

/**
 *  Adds the item to the cart and alters the button's appearance to indicate the product has been added
 */
- (IBAction)addItemToCart:(id)sender;

@end

@implementation PWADemoProductDetailsViewController

#pragma mark - Display

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.isAddedToCart = NO;
    
    [self displayProductDetails];
    
    [self updateView];
}

- (void)displayProductDetails
{
    if (self.selectedProduct) {
        self.productQuantitySelected = 1;
        
        self.productQuantityLabel.text = [NSString stringWithFormat:@"%ld", (long)self.productQuantitySelected];
        self.productImageView.image = [UIImage imageNamed:self.selectedProduct.productImageName];
        self.productNameLabel.text = self.selectedProduct.productName;
        self.productColorLabel.text = self.selectedProduct.productColor;
        self.productPriceLabel.text = [PWADemoCurrencyFormatter stringFromNumber:self.selectedProduct.unitPrice];
    }
}

- (void)displayButtonsBeforeProductAdded
{
    [self.addToCartButton setTitle:@"ADD TO CART" forState:UIControlStateNormal];
    self.addToCartButton.backgroundColor = [UIColor torquoiseColor];
    [self.incrementQuantityButton setTitleColor:[UIColor torquoiseColor] forState:UIControlStateNormal];
    [self.decrementQuantityButton setTitleColor:[UIColor torquoiseColor] forState:UIControlStateNormal];
}

- (void)displayButtonsAfterProductAdded
{
    char addedToCartText[] = "\u2713 ADDED TO CART"; // \u2713 is a unicode checkmark
    NSData *addedToCartTextData = [NSData dataWithBytes:addedToCartText length:strlen(addedToCartText)];
    [self.addToCartButton setTitle:[[NSString alloc] initWithData:addedToCartTextData encoding:NSUTF8StringEncoding] forState:UIControlStateNormal];
    self.addToCartButton.backgroundColor = [UIColor limeGreenColor];
    [self.incrementQuantityButton setTitleColor:[UIColor limeGreenColor] forState:UIControlStateNormal];
    [self.decrementQuantityButton setTitleColor:[UIColor limeGreenColor] forState:UIControlStateNormal];
}

- (void)updateView
{
    self.productQuantityLabel.text = [NSString stringWithFormat:@"%ld", (long)self.productQuantitySelected];
    
    [self.cartBarButtonItem updateCartQuantity];
    
    if (self.isAddedToCart) {
        [self displayButtonsAfterProductAdded];
    } else {
        [self displayButtonsBeforeProductAdded];
    }
}

#pragma mark - Product

- (IBAction)incrementQuantity:(id)sender
{
    self.productQuantitySelected++;
    self.isAddedToCart = NO;
    [self updateView];
}

- (IBAction)decrementQuantity:(id)sender
{
    self.productQuantitySelected -= (self.productQuantitySelected > 1) ? 1 : 0;
    self.isAddedToCart = NO;
    [self updateView];
}

#pragma mark - Cart

- (IBAction)addItemToCart:(id)sender
{
    if (!self.isAddedToCart) {
        [[PWADemoCart sharedCart] addItemToCart:self.selectedProduct withQuantity:self.productQuantitySelected];
    }
    
    self.isAddedToCart = !self.isAddedToCart;
    
    [self updateView];
}

@end
