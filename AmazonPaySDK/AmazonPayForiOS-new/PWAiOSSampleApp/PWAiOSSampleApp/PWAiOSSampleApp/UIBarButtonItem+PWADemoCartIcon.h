//
//  UIBarButtonItem+PWADemoCartIcon.h
//  PWAiOSSampleApp
//
//  Copyright © 2016 Amazon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWADemoCart.h"
#import "UIColor+PWADemoColors.h"

/**
 *  The PWADemoCartIcon category of UIBarButtonItem provides a method to adjust the
 *  the cart icon based on the number of distinct items in the cart
 */
@interface UIBarButtonItem (PWADemoCartIcon)

/**
 *  Updates the cart icon bar button item with the current number of distinct items in the cart
 */
- (void)updateCartQuantity;

@end
