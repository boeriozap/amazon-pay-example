//
//  ViewController.swift
//  AmazonPayProject
//
//  Created by Alessio Boerio on 07/02/2018.
//  Copyright © 2018 Alessio Boerio. All rights reserved.
//

import UIKit
import LoginWithAmazon
import PayWithAmazon

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginButtonDidTap(_ sender: Any) {
        // Build an authorize request.
        let authorizationRequest = AMZNAuthorizeRequest()
        authorizationRequest.scopes = [AMZNProfileScope.profile(),
                                       APayPaymentScope.initiate(),
                                       APayPaymentScope.shippingAddress()]
        // Set this strategy if you want to check the buyer's login status and return an authentication error instead of opening a login page if the buyer didn’t sign in.
        authorizationRequest.interactiveStrategy = AMZNInteractiveStrategy.never
        
        // Make an Authorize call to the Login with Amazon SDK.
        AMZNAuthorizationManager.shared().authorize(authorizationRequest) { (result, userDidCancel, error) in
            if error != nil {
                // Handle the error from either the SDK or authorization.
            } else if userDidCancel {
                // Handle the error caused when the buyer selected to cancel
                // when logging on using Login with Amazon.
            } else {
                // The authentication was successful.
                // Obtain the access token and buyer information.
                let accessToken = result?.token
                let user = result?.user
                let userID = user?.userID
            }
        }
    }
}

